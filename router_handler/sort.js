// 分类管理 sys_sort sys_sort_sort
const db = require('../db')
const { getNowTime, formatTime } = require('../utils/index')
// 集成log4js日志
const log4js = require('../utils/log4js')
const logger = log4js.getLogger('runtime')

/**
 * 新增分类
 * @param {*} req
 * @param {*} res
 */
exports.addSort = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 查询
  const selectSql = `select * from sys_sort where is_delete = 0 and (code = ? or name = ?)`
  db.query(selectSql, [req.body.code, req.body.name], (err, results) => {
    if (err) return res.new_send(err)
    // 判断分类名称和分类编码是否存在
    if (results.length >= 2) return res.new_send('分类名称与分类编码已存在')
    if (results.length === 1) {
      const { code, name } = results[0]
      const { code: sort_code, name: sort_name } = req.body
      if (code === sort_code && name === sort_name) {
        return res.new_send('分类名称与分类编码已存在')
      }
      if (name === sort_name) {
        return res.new_send('分类名称已存在')
      }
      if (code === sort_code) {
        return res.new_send('分类编码已存在')
      }
    }
    // 插入数据库
    const insertSql = `insert into sys_sort set ?`
    db.query(insertSql, req.body, (err, results) => {
      if (err) return res.new_send(err)
      if (results.affectedRows !== 1) return res.new_send('新增分类失败')
      res.send({
        status: 0,
        message: '新增分类成功',
        data: { insertId: results.insertId }
      })
    })
  })
}

/**
 * 修改分类
 * @param {*} req
 * @param {*} res
 */
exports.updateSort = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 查询
  const selectSql = `select * from sys_sort where id <> ? and is_delete = 0 and (code = ? or name = ?)`
  db.query(
    selectSql,
    [req.body.id, req.body.code, req.body.name],
    (err, results) => {
      if (err) return res.new_send(err)
      // 判断分类名称和分类编码是否存在
      if (results.length >= 2) return res.new_send('分类名称与分类编码已存在')
      if (results.length === 1) {
        const { code, name } = results[0]
        const { code: sort_code, name: sort_name } = req.body
        if (code === sort_code && name === sort_name) {
          return res.new_send('分类名称与分类编码已存在')
        }
        if (name === sort_name) {
          return res.new_send('分类名称已存在')
        }
        if (code === sort_code) {
          return res.new_send('分类编码已存在')
        }
      }
      //  修改数据库
      const updateSql = `update sys_sort set ? where Id = ?`
      db.query(updateSql, [req.body, req.body.id], (err, results) => {
        if (err) return res.new_send(err)
        if (results.affectedRows !== 1) return res.new_send('更新分类失败')
        res.new_send('更新分类成功', 0)
      })
    }
  )
}

/**
 * 删除分类
 * @param {*} req
 * @param {*} res
 */
exports.deleteSortById = (req, res) => {
  const deleteSql = `update sys_sort set is_delete = 1 where id = ?`
  db.query(deleteSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows <= 0) return res.new_send('删除分类失败')
    res.new_send('删除分类成功', 0)
  })
}

/**
 * 获取分类列表
 * @param {*} req
 * @param {*} res
 */
exports.getSortLsit = (req, res) => {
  // 分页加载
  const page_num = req.query.page_num || 1 //当前的num
  const page_size = req.query.page_size || 10 //当前页的数量
  const name = req.query.name
  const params = [
    (parseInt(page_num) - 1) * parseInt(page_size),
    parseInt(page_size)
  ]
  // 查询
  const selectSql =
    `select a.*, b.username user_name from sys_sort as a left join sys_user as b on a.user_id = b.id where a.is_delete = 0` +
    (name ? ` and name like '%${name}%'` : '') +
    ` limit ?,?`
  db.query(selectSql, params, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
      })
    }
    // 列表数据
    let sorts = results

    // 查询数据库总数
    const totalSql =
      `select count(*) as total from sys_sort where is_delete = 0` +
      (name ? ` and name like '%${name}%'` : '')
    db.query(totalSql, (err, results) => {
      if (err) return res.new_send(err)
      let total = results[0]['total'] // 总数
      res.send({
        status: 0,
        message: '获取分类列表成功',
        page_num,
        page_size,
        total,
        data: sorts
      })
    })
  })
}

/**
 * 获取分类信息
 * @param {*} req
 * @param {*} res
 */
exports.getSortById = (req, res) => {
  const selectSql = `select * from sys_sort where id = ?`
  db.query(selectSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
      })
    }
    res.send({
      status: 0,
      message: '获取分类信息成功',
      data: results
    })
  })
}

/**
 * 新增分类关联标签
 * @param {*} req
 * @param {*} res
 */
exports.addSortInTag = async (req, res) => {
  if (req.body.length > 0) {
    const selectSql = `select * from sys_sort_tag where sort_id = ? and tag_id = ?`
    const insertSql = `insert into sys_sort_tag set ?`
    req.body.map(async (item) => {
      // 查询是否存在
      await db.query(selectSql, [item.sort_id, item.tag_id], (err, results) => {
        if (err) return res.new_send(err)
        if (results.length <= 0) {
          // 当数据不存在，直接新增
          db.query(insertSql, item, (err, results) => {
            if (err) return res.new_send(err)
            logger.info(
              `新增分类(${item.sort_id})关联标签(${item.tag_id})成功...`
            )
          })
        } else {
          // 当数据存在，直接修改删除状态
          const deleteSql = `update sys_sort_tag set is_delete = 0 where sort_id = ? and tag_id = ?`
          db.query(deleteSql, [item.sort_id, item.tag_id], (err, results) => {
            if (err) return res.new_send(err)
            logger.info(
              `新增分类(${item.sort_id})关联标签(${item.tag_id})成功...`
            )
          })
        }
      })
    })
    await res.send({
      status: 0,
      message: '分类关联标签成功'
    })
  } else {
    res.new_send('分类关联标签失败')
  }
}

/**
 * 根据Id删除分类关联标签
 * @param {*} req
 * @param {*} res
 */
exports.deleteSortInTagById = (req, res) => {
  const deleteSql = `update sys_sort_tag set is_delete = 1 where sort_id = ? and tag_id = ?`
  db.query(deleteSql, [req.body.sort_id, req.body.tag_id], (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows <= 0) return res.new_send('删除分类关联标签失败')
    res.send({
      status: 0,
      message: '删除分类关联标签成功'
    })
  })
}

/**
 * 根据分类Id查询标签列表
 * @param {*} req
 * @param {*} res
 */
exports.getSortInTagList = (req, res) => {
  const selectSql = `select a.*, b.tag_id, b.sort_id, 
  case when b.sort_id is not null then 1 else 0 end is_relevance from sys_tag as a
  left join (
    select * from sys_sort_tag where is_delete = 0 and sort_id = ?
  ) as b on a.id = b.tag_id where a.is_delete = 0`
  db.query(selectSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    res.send({
      status: 0,
      message: '获取分类关联标签列表成功',
      data: results
    })
  })
}

/**
 * 根据目录Id获取分类列表
 * @param {*} req
 * @param {*} res
 */
exports.getSortLsitByCatalogId = (req, res) => {
  // 查询
  const selectSql =
    `select distinct a.*, b.username user_name from sys_sort as a left join sys_user as b on a.user_id = b.id 
    left join sys_catalog_sort as c on a.id = c.sort_id
    where a.is_delete = 0 and c.is_delete = 0 and c.catalog_id in (${ req.query.id })`
  db.query(selectSql, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
      })
    }
    res.send({
      status: 0,
      message: '获取分类列表成功',
      data: results
    })
  })
}
