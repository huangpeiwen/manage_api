// 代码片段 blog_code
const db = require('../db')
const { getNowTime, formatTime } = require('../utils/index')

/**
 * 新增代码片段
 * @param {*} req
 * @param {*} res
 */
exports.addCode = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 插入数据库
  const insertSql = `insert into blog_code set ?`
  db.query(insertSql, req.body, (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows !== 1) return res.new_send('新增代码片段失败')
    res.send({
      status: 0,
      message: '新增代码片段成功',
      data: { insertId: results.insertId }
    })
  })
}

/**
 * 修改代码片段
 * @param {*} req
 * @param {*} res
 */
exports.updateCode = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 修改数据库
  const updateSql = `update blog_code set ? where id = ?`
  db.query(updateSql, [req.body, req.body.id], (err, results) => {
    console.log(err, results)
    if (err) return res.new_send(err)
    if (results.affectedRows !== 1) return res.new_send('更新代码片段失败')
    res.new_send('更新代码片段成功', 0)
  })
}

/**
 * 删除代码片段
 * @param {*} req
 * @param {*} res
 */
exports.deleteCodeById = (req, res) => {
  const deleteSql = `update blog_code set is_delete = 1 where id = ?`
  db.query(deleteSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows <= 0) return res.new_send('删除代码片段失败')
    res.new_send('删除代码片段成功', 0)
  })
}

/**
 * 获取笔记列表
 * @param {*} req
 * @param {*} res
 */
exports.getCodeLsit = (req, res) => {
  // 分页加载
  console.log(req.query, 'req.query')
  const page_num = req.query.page_num || 1 //当前页
  const page_size = req.query.page_size || 10 //当前页的数量
  const name = req.query.name
  const params = [
    (parseInt(page_num) - 1) * parseInt(page_size),
    parseInt(page_size)
  ]
  // 查询
  const selectSql =
    `select a.*, 
    (
        select GROUP_CONCAT(ct.name) as names from sys_catalog as ct where CONCAT(a.catalog_ids, "," ) like CONCAT('%', ct.id, ",", '%') 
    ) as catalog_names,
    (
        select GROUP_CONCAT(st.name) as names from sys_sort as st where CONCAT(a.sort_ids, "," ) like CONCAT('%', st.id, ",", '%') 
    ) as sort_names,
    (
        select GROUP_CONCAT(cg.name) as names from sys_tag as cg where CONCAT(a.sort_ids, "," ) like CONCAT('%', cg.id, ",", '%') 
    ) as tag_names, b.username user_name
    from blog_code as a left join sys_user as b on a.user_id = b.id where a.is_delete = 0` +
    (name ? ` and a.name like '%${name}%'` : '') +
    ` limit ?,?`
  db.query(selectSql, params, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
      })
    }
    // 列表数据
    let websites = results

    // 查询数据库总数
    const totalSql =
      'select count(*) as total from blog_code where is_delete = 0' +
      (name ? ` and name like '%${name}%'` : '')
    db.query(totalSql, (err, results) => {
      if (err) return res.new_send(err)
      let total = results[0]['total'] // 总数
      res.send({
        status: 0,
        message: '获取代码片段列表成功',
        page_num,
        page_size,
        total,
        data: websites
      })
    })
  })
}

/**
 * 获取代码片段
 * @param {*} req
 * @param {*} res
 */
exports.getCodeById = (req, res) => {
  const selectSql = `select a.*, 
  (
      select GROUP_CONCAT(ct.name) as names from sys_catalog as ct where CONCAT(a.catalog_ids, "," ) like CONCAT('%', ct.id, ",", '%') 
  ) as catalog_names,
  (
      select GROUP_CONCAT(st.name) as names from sys_sort as st where CONCAT(a.sort_ids, "," ) like CONCAT('%', st.id, ",", '%') 
  ) as sort_names,
  (
      select GROUP_CONCAT(cg.name) as names from sys_tag as cg where CONCAT(a.sort_ids, "," ) like CONCAT('%', cg.id, ",", '%') 
  ) as tag_names, b.username user_name
  from blog_code as a left join sys_user as b on a.user_id = b.id where a.id = ?`
  db.query(selectSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
      })
    }
    res.send({
      status: 0,
      message: '获取代码片段成功',
      data: results
    })
  })
}

/**
 * 新增代码片段从表
 * @param {*} req
 * @param {*} res
 */
exports.addCodeUrl = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 插入数据库
  const insertSql = `insert into blog_code_url set ?`
  db.query(insertSql, req.body, (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows !== 1) return res.new_send('新增代码片段从表失败')
    res.send({
      status: 0,
      message: '新增代码片段从表成功',
      data: { insertId: results.insertId }
    })
  })
}

/**
 * 修改代码片段从表
 * @param {*} req
 * @param {*} res
 */
exports.updateCodeUrl = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 修改数据库
  const updateSql = `update blog_code_url set ? where id = ?`
  db.query(updateSql, [req.body, req.body.id], (err, results) => {
    console.log(err, results)
    if (err) return res.new_send(err)
    if (results.affectedRows !== 1) return res.new_send('更新代码片段从表失败')
    res.new_send('更新代码片段从表成功', 0)
  })
}

/**
 * 删除代码片段从表
 * @param {*} req
 * @param {*} res
 */
exports.deleteCodeUrlById = (req, res) => {
  const deleteSql = `update blog_code_url set is_delete = 1 where id = ?`
  db.query(deleteSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows <= 0) return res.new_send('删除代码片段从表失败')
    res.new_send('删除代码片段从表成功', 0)
  })
}

/**
 * 获取代码片段从表列表
 * @param {*} req
 * @param {*} res
 */
exports.getCodeUrlLsit = (req, res) => {
  const code_id = req.query.code_id
  const selectSql =
    `select * from blog_code_url where is_delete = 0` +
    (code_id ? ` and code_id = ?` : '')
  db.query(selectSql, code_id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
      })
    }
    res.send({
      status: 0,
      message: '获取代码片段从表列表成功',
      data: results
    })
  })
}

/**
 * 获取代码片段从表
 * @param {*} req
 * @param {*} res
 */
exports.getCodeUrlById = (req, res) => {
  const selectSql = `select * from blog_code_url where id = ?`
  db.query(selectSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
      })
    }
    res.send({
      status: 0,
      message: '获取代码片段从表成功',
      data: results
    })
  })
}
