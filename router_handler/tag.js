// 标签管理 sys_tag
const db = require('../db')
const { getNowTime, formatTime } = require('../utils/index')

/**
 * 新增标签
 * @param {*} req
 * @param {*} res
 */
exports.addTag = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 查询
  const selectSql = `select * from sys_tag where is_delete = 0 and (code = ? or name = ?)`
  db.query(selectSql, [req.body.code, req.body.name], (err, results) => {
    if (err) return res.new_send(err)
    // 判断标签名称和标签编码是否存在
    if (results.length >= 2) return res.new_send('标签名称与标签编码已存在')
    if (results.length === 1) {
      const { code, name } = results[0]
      const { code: tag_code, name: tag_name } = req.body
      if (code === tag_code && name === tag_name) {
        return res.new_send('标签名称与标签编码已存在')
      }
      if (name === tag_name) {
        return res.new_send('标签名称已存在')
      }
      if (code === tag_code) {
        return res.new_send('标签编码已存在')
      }
    }
    // 插入数据库
    const insertSql = `insert into sys_tag set ?`
    db.query(insertSql, req.body, (err, results) => {
      if (err) return res.new_send(err)
      if (results.affectedRows !== 1) return res.new_send('新增标签失败')
      res.send({
        status: 0,
        message: '新增标签成功',
        data: { insertId: results.insertId }
      })
    })
  })
}

/**
 * 修改标签
 * @param {*} req
 * @param {*} res
 */
exports.updateTag = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 查询
  const selectSql = `select * from sys_tag where id <> ? and is_delete = 0 and (code = ? or name = ?)`
  db.query(
    selectSql,
    [req.body.id, req.body.code, req.body.name],
    (err, results) => {
      if (err) return res.new_send(err)
      // 判断标签名称和标签编码是否存在
      if (results.length >= 2) return res.new_send('标签名称与标签编码已存在')
      if (results.length === 1) {
        const { code, name } = results[0]
        const { code: tag_code, name: tag_name } = req.body
        if (code === tag_code && name === tag_name) {
          return res.new_send('标签名称与标签编码已存在')
        }
        if (name === tag_name) {
          return res.new_send('标签名称已存在')
        }
        if (code === tag_code) {
          return res.new_send('标签编码已存在')
        }
      }
      // 修改数据库
      const updateSql = `update sys_tag set ? where id = ?`
      db.query(updateSql, [req.body, req.body.id], (err, results) => {
        if (err) return res.new_send(err)
        if (results.affectedRows !== 1) return res.new_send('更新标签失败')
        //todo: 处理操作日志表
        res.new_send('更新标签成功', 0)
      })
    }
  )
}

/**
 * 删除标签
 * @param {*} req
 * @param {*} res
 */
exports.deleteTagById = (req, res) => {
  const deleteSql = `update sys_tag set is_delete = 1 where id = ?`
  db.query(deleteSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows <= 0) return res.new_send('删除标签失败')
    res.new_send('删除标签成功', 0)
  })
}

/**
 * 获取标签列表
 * @param {*} req
 * @param {*} res
 */
exports.getTagLsit = (req, res) => {
  // 分页加载
  const page_num = req.query.page_num || 1 //当前的num
  const page_size = req.query.page_size || 10 //当前页的数量
  const name = req.query.name
  const params = [
    (parseInt(page_num) - 1) * parseInt(page_size),
    parseInt(page_size)
  ]
  // 查询
  const selectSql =
    `select a.*, b.username user_name from sys_tag as a left join sys_user as b on a.user_id = b.id where a.is_delete = 0` +
    (name ? ` and a.name like '%${name}%'` : '') +
    ` limit ?,?`
  db.query(selectSql, params, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
      })
    }
    // 列表数据
    let tags = results

    // 查询数据库总数
    const totalSql =
      `select count(*) as total from sys_tag where is_delete = 0` +
      (name ? ` and name like '%${name}%'` : '')
    db.query(totalSql, (err, results) => {
      if (err) return res.new_send(err)
      let total = results[0]['total'] // 总数
      res.send({
        status: 0,
        message: '获取标签列表成功',
        page_num,
        page_size,
        total,
        data: tags
      })
    })
  })
}

/**
 * 获取标签信息
 * @param {*} req
 * @param {*} res
 */
exports.getTagById = (req, res) => {
  const selectSql = `select a.*, b.username user_name from sys_tag as a left join sys_user as b on a.user_id = b.id where a.id = ?`
  db.query(selectSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
      })
    }
    res.send({
      status: 0,
      message: '获取标签信息成功',
      data: results
    })
  })
}

/**
 * 根据分类Id获取标签列表
 * @param {*} req
 * @param {*} res
 */
exports.getTagLsitBySortId = (req, res) => {
  // 查询
  const selectSql =
    `select distinct a.*, b.username user_name from sys_tag as a left join sys_user as b on a.user_id = b.id 
    left join sys_sort_tag as c on a.id = c.tag_id
    where a.is_delete = 0 and c.is_delete = 0 and c.sort_id in (${ req.query.id })`
  console.log(selectSql, 'selectSql')
  db.query(selectSql, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
      })
    }
    res.send({
      status: 0,
      message: '获取分类列表成功',
      data: results
    })
  })
}
