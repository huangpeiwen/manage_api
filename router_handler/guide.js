// 商品导购 buy_guide
const db = require('../db')
const { getNowTime, formatTime } = require('../utils/index')

/**
 * 新增商品导购
 * @param {*} req
 * @param {*} res
 */
exports.addGuide = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 查询是否存在
  const selectSql = `select * from buy_guide where is_delete = 0 and good_name = ?`
  db.query(selectSql, req.body.good_name, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) return res.new_send('商品已存在')
    // 插入数据库
    const insertSql = `insert into buy_guide set ?`
    db.query(insertSql, req.body, (err, results) => {
      if (err) return res.new_send(err)
      if (results.affectedRows !== 1) return res.new_send('新增商品导购失败')
      res.send({
        status: 0,
        message: '新增商品导购成功',
        data: { insertId: results.insertId }
      })
    })
  })
}

/**
 * 修改商品导购
 * @param {*} req
 * @param {*} res
 */
exports.updateGuide = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 查询是否存在
  const selectSql = `select * from buy_guide where id <> ? and is_delete = 0 and good_name = ?`
  db.query(selectSql, [req.body.id, req.body.good_name], (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) return res.new_send('商品已存在')
    // 修改数据库
    const updateSql = `update buy_guide set ? where id = ?`
    db.query(updateSql, [req.body, req.body.id], (err, results) => {
      if (err) return res.new_send(err)
      if (results.affectedRows !== 1) return res.new_send('更新商品导购失败')
      res.new_send('更新商品导购成功', 0)
    })
  })
}

/**
 * 删除商品导购
 * @param {*} req
 * @param {*} res
 */
exports.deleteGuideById = (req, res) => {
  const deleteSql = `update buy_guide set is_delete = 1 where id = ?`
  db.query(deleteSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows <= 0) return res.new_send('删除商品导购失败')
    res.new_send('删除商品导购成功', 0)
  })
}

/**
 * 获取商品导购列表
 * @param {*} req
 * @param {*} res
 */
exports.getGuideLsit = (req, res) => {
  // 分页加载
  const page_num = req.query.page_num || 1 //当前页
  const page_size = req.query.page_size || 10 //当前页的数量
  const good_name = req.query.good_name
  const params = [
    (parseInt(page_num) - 1) * parseInt(page_size),
    parseInt(page_size)
  ]
  // 查询
  const selectSql =
    `select a.*, b.username user_name from buy_guide as a left join sys_user as b on a.user_id = b.id where a.is_delete = 0` +
    (good_name ? ` and a.good_name like '%${good_name}%'` : '') +
    ` limit ?,?`
  db.query(selectSql, params, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
      })
    }
    // 列表数据
    let guides = results

    // 查询数据库总数
    const totalSql =
      'select count(*) as total from buy_guide where is_delete = 0' +
      (good_name ? ` and good_name like '%${good_name}%'` : '')
    db.query(totalSql, (err, results) => {
      if (err) return res.new_send(err)
      let total = results[0]['total'] // 总数
      res.send({
        status: 0,
        message: '获取商品导购列表成功',
        page_num,
        page_size,
        total,
        data: guides
      })
    })
  })
}

/**
 * 获取商品导购
 * @param {*} req
 * @param {*} res
 */
exports.getGuideById = (req, res) => {
  const selectSql = `select a.*, b.username user_name from buy_guide as a left join sys_user as b on a.user_id = b.id where a.is_delete = 0 and a.id = ?`
  db.query(selectSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
      })
    }
    res.send({
      status: 0,
      message: '获取商品导购成功',
      data: results
    })
  })
}
