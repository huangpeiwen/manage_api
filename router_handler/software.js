// 软件管理 blog_software
const db = require('../db')
const { getNowTime, formatTime } = require('../utils/index')

/**
 * 新增软件管理
 * @param {*} req
 * @param {*} res
 */
exports.addSoftware = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 查询是否存在
  const selectSql = `select * from blog_software where is_delete = 0 and name = ?`
  db.query(selectSql, req.body.name, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) return res.new_send('软件名称已存在')
    // 插入数据库
    const insertSql = `insert into blog_software set ?`
    db.query(insertSql, req.body, (err, results) => {
      if (err) return res.new_send(err)
      if (results.affectedRows !== 1) return res.new_send('新增软件管理失败')
      res.send({
        status: 0,
        message: '新增软件管理成功',
        data: { insertId: results.insertId }
      })
    })
  })
}

/**
 * 修改软件管理
 * @param {*} req
 * @param {*} res
 */
exports.updateSoftware = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 查询是否存在
  const selectSql = `select * from blog_software where id <> ? and is_delete = 0 and name = ?`
  db.query(selectSql, [req.body.id, req.body.name], (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) return res.new_send('软件名称已存在')
    // 修改数据库
    const updateSql = `update blog_software set ? where id = ?`
    db.query(updateSql, [req.body, req.body.id], (err, results) => {
      if (err) return res.new_send(err)
      if (results.affectedRows !== 1) return res.new_send('更新软件管理失败')
      res.new_send('更新软件管理成功', 0)
    })
  })
}

/**
 * 删除软件管理
 * @param {*} req
 * @param {*} res
 */
exports.deleteSoftwareById = (req, res) => {
  const deleteSql = `update blog_software set is_delete = 1 where id = ?`
  db.query(deleteSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows <= 0) return res.new_send('删除软件管理失败')
    res.new_send('删除软件管理成功', 0)
  })
}

/**
 * 获取软件管理列表
 * @param {*} req
 * @param {*} res
 */
exports.getSoftwareLsit = (req, res) => {
  // 分页加载
  const page_num = req.query.page_num || 1 //当前页
  const page_size = req.query.page_size || 10 //当前页的数量
  const name = req.query.name
  const params = [
    (parseInt(page_num) - 1) * parseInt(page_size),
    parseInt(page_size)
  ]
  // 查询
  const selectSql =
    `select a.*, b.username user_name from blog_software as a left join sys_user as b on a.user_id = b.id where a.is_delete = 0` +
    (name ? ` and a.name like '%${name}%'` : '') +
    ` limit ?,?`
  db.query(selectSql, params, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
      })
    }
    // 列表数据
    let softwares = results

    // 查询数据库总数
    const totalSql =
      'select count(*) as total from blog_software where is_delete = 0' +
      (name ? ` and name like '%${name}%'` : '')
    db.query(totalSql, (err, results) => {
      if (err) return res.new_send(err)
      let total = results[0]['total'] // 总数
      res.send({
        status: 0,
        message: '获取软件管理列表成功',
        page_num,
        page_size,
        total,
        data: softwares
      })
    })
  })
}

/**
 * 获取软件管理
 * @param {*} req
 * @param {*} res
 */
exports.getSoftwareById = (req, res) => {
  const selectSql = `select a.*, b.username user_name from blog_software as a left join sys_user as b on a.user_id = b.id where a.is_delete = 0 and a.id = ?`
  db.query(selectSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
      })
    }
    res.send({
      status: 0,
      message: '获取软件管理成功',
      data: results
    })
  })
}
