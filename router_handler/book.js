// 书籍推荐 blog_book
const db = require('../db')
const { getNowTime, formatTime } = require('../utils/index')

/**
 * 新增书籍推荐
 * @param {*} req
 * @param {*} res
 */
exports.addBook = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 查询是否存在
  const selectSql = `select * from blog_book where is_delete = 0 and name = ?`
  db.query(selectSql, req.body.name, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) return res.new_send('书名已存在')
    // 插入数据库
    const insertSql = `insert into blog_book set ?`
    db.query(insertSql, req.body, (err, results) => {
      if (err) return res.new_send(err)
      if (results.affectedRows !== 1) return res.new_send('新增书籍推荐失败')
      res.send({
        status: 0,
        message: '新增书籍推荐成功',
        data: { insertId: results.insertId }
      })
    })
  })
}

/**
 * 修改书籍推荐
 * @param {*} req
 * @param {*} res
 */
exports.updateBook = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 查询是否存在
  const selectSql = `select * from blog_book where id <> ? and is_delete = 0 and name = ?`
  db.query(selectSql, [req.body.id, req.body.name], (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) return res.new_send('书名已存在')
    // 修改数据库
    const updateSql = `update blog_book set ? where id = ?`
    db.query(updateSql, [req.body, req.body.id], (err, results) => {
      if (err) return res.new_send(err)
      if (results.affectedRows !== 1) return res.new_send('更新书籍推荐失败')
      res.new_send('更新书籍推荐成功', 0)
    })
  })
}

/**
 * 删除书籍推荐
 * @param {*} req
 * @param {*} res
 */
exports.deleteBookById = (req, res) => {
  const deleteSql = `update blog_book set is_delete = 1 where id = ?`
  db.query(deleteSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows <= 0) return res.new_send('删除书籍推荐失败')
    res.new_send('删除书籍推荐成功', 0)
  })
}

/**
 * 获取书籍推荐列表
 * @param {*} req
 * @param {*} res
 */
exports.getBookLsit = (req, res) => {
  // 分页加载
  const page_num = req.query.page_num || 1 //当前页
  const page_size = req.query.page_size || 10 //当前页的数量
  const name = req.query.name
  const params = [
    (parseInt(page_num) - 1) * parseInt(page_size),
    parseInt(page_size)
  ]
  // 查询
  // const selectSql = `select * from blog_book where is_delete = 0` +
  const selectSql =
    `select a.*, 
    (
        select GROUP_CONCAT(ct.name) as names from sys_catalog as ct where CONCAT(a.catalog_ids, "," ) like CONCAT('%', ct.id, ",", '%') 
    ) as catalog_names,
    (
        select GROUP_CONCAT(st.name) as names from sys_sort as st where CONCAT(a.sort_ids, "," ) like CONCAT('%', st.id, ",", '%') 
    ) as sort_names,
    (
        select GROUP_CONCAT(cg.name) as names from sys_tag as cg where CONCAT(a.sort_ids, "," ) like CONCAT('%', cg.id, ",", '%') 
    ) as tag_names, b.username user_name
    from blog_book as a left join sys_user as b on a.user_id = b.id where a.is_delete = 0` +
    (name ? ` and a.name like '%${name}%'` : '') +
    ` limit ?,?`
  db.query(selectSql, params, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
      })
    }
    // 列表数据
    let books = results

    // 查询数据库总数
    const totalSql =
      'select count(*) as total from blog_book where is_delete = 0' +
      (name ? ` and name like '%${name}%'` : '')
    db.query(totalSql, (err, results) => {
      if (err) return res.new_send(err)
      let total = results[0]['total'] // 总数
      res.send({
        status: 0,
        message: '获取书籍推荐列表成功',
        page_num,
        page_size,
        total,
        data: books
      })
    })
  })
}

/**
 * 获取书籍推荐
 * @param {*} req
 * @param {*} res
 */
exports.getBookById = (req, res) => {
  // const selectSql = `select * from blog_book where id = ?`
  const selectSql = `select a.*, 
  (
      select GROUP_CONCAT(ct.name) as names from sys_catalog as ct where CONCAT(a.catalog_ids, "," ) like CONCAT('%', ct.id, ",", '%') 
  ) as catalog_names,
  (
      select GROUP_CONCAT(st.name) as names from sys_sort as st where CONCAT(a.sort_ids, "," ) like CONCAT('%', st.id, ",", '%') 
  ) as sort_names,
  (
      select GROUP_CONCAT(cg.name) as names from sys_tag as cg where CONCAT(a.sort_ids, "," ) like CONCAT('%', cg.id, ",", '%') 
  ) as tag_names, b.username user_name
  from blog_book as a left join sys_user as b on a.user_id = b.id where a.is_delete = 0 and a.id = ?`
  db.query(selectSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
      })
    }
    res.send({
      status: 0,
      message: '获取书籍推荐成功',
      data: results
    })
  })
}
