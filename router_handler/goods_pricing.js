// 未购计价 buy_goods_pricing
const db = require('../db')
const { getNowTime, formatTime } = require('../utils/index')

/**
 * 新增未购计价
 * @param {*} req
 * @param {*} res
 */
exports.addGoodsPricing = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 插入数据库
  const insertSql = `insert into buy_goods_pricing set ?`
  db.query(insertSql, req.body, (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows !== 1) return res.new_send('新增未购计价失败')
    res.send({
      status: 0,
      message: '新增未购计价成功',
      data: { insertId: results.insertId }
    })
  })
}

/**
 * 修改未购计价
 * @param {*} req
 * @param {*} res
 */
exports.updateGoodsPricing = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 修改数据库
  const updateSql = `update buy_goods_pricing set ? where id = ?`
  db.query(updateSql, [req.body, req.body.id], (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows !== 1) return res.new_send('更新未购计价失败')
    res.new_send('更新未购计价成功', 0)
  })
}

/**
 * 删除未购计价
 * @param {*} req
 * @param {*} res
 */
exports.deleteGoodsPricingById = (req, res) => {
  const deleteSql = `update buy_goods_pricing set is_delete = 1 where id = ?`
  db.query(deleteSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows <= 0) return res.new_send('删除未购计价失败')
    res.new_send('删除未购计价成功', 0)
  })
}

/**
 * 获取未购计价列表
 * @param {*} req
 * @param {*} res
 */
exports.getGoodsPricingLsit = (req, res) => {
  // 分页加载
  const page_num = req.query.page_num || 1 //当前页
  const page_size = req.query.page_size || 10 //当前页的数量
  const good_name = req.query.good_name
  const params = [
    (parseInt(page_num) - 1) * parseInt(page_size),
    parseInt(page_size)
  ]
  // 查询
  const selectSql =
    `select b.*, a.id as pricingId, a.current_price, a.current_time, a.create_time as pricing_time, c.username user_name from buy_goods_pricing as a
  left join buy_guide as b on a.guide_id = b.id 
  left join sys_user as c on b.user_id = c.id
  where a.is_delete = 0` +
    (good_name ? ` and b.good_name like '%${good_name}%'` : '') +
    ` limit ?,?`
  db.query(selectSql, params, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
        item.pricing_time = formatTime(item.pricing_time)
      })
    }
    // 列表数据
    let pricings = results

    // 查询数据库总数
    const totalSql =
      'select count(*) as total from buy_goods_pricing as a left join buy_guide as b on a.guide_id = b.id where a.is_delete = 0' +
      (good_name ? ` and b.good_name like '%${good_name}%'` : '')
    db.query(totalSql, (err, results) => {
      if (err) return res.new_send(err)
      let total = results[0]['total'] // 总数
      res.send({
        status: 0,
        message: '获取未购计价列表成功',
        page_num,
        page_size,
        total,
        data: pricings
      })
    })
  })
}

/**
 * 获取未购计价
 * @param {*} req
 * @param {*} res
 */
exports.getGoodsPricingById = (req, res) => {
  const selectSql = `select b.*, a.id as pricingId, a.current_price, a.current_time, a.create_time as pricing_time, c.username user_name from buy_goods_pricing as a
  left join buy_guide as b on a.guide_id = b.id 
  left join sys_user as c on b.user_id = c.id
  where a.id = ?`
  db.query(selectSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
        item.flow_time = formatTime(item.flow_time)
      })
    }
    res.send({
      status: 0,
      message: '获取未购计价成功',
      data: results
    })
  })
}
