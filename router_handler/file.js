// 文件库 sys_file
const db = require('../db')
const { getNowTime, formatTime } = require('../utils/index')

/**
 * 新增文件数据
 * @param {*} req
 * @param {*} res
 */
exports.addFile = (req, res) => {
  // let file = {
  //   biz_id: "", // 业务主表ID
  //   file_url: "", // 文件路径
  //   file_name: "", // 文件名称
  //   file_type: "", // 文件类型
  //   file_size: 0, // 文件大小
  //   file_extname: "", // 文件扩展名
  //   user_id: "", // 创建人Id
  //   create_time: getNowTime(), // 创建时间
  //   remark: "", // 备注
  // };
  // // 插入数据库
  // const insertSql = `insert into sys_file set ?`;
  // db.query(insertSql, file, (err, results) => {
  //   if (err) return res.new_send(err);
  //   if (results.affectedRows !== 1) return res.new_send("新增文件失败");
  //   res.send({
  //     status: 0,
  //     message: "新增文件成功",
  //     data: { insertId: results.insertId },
  //   });
  // });
  res.send('OK')
}

/**
 * 批量新增文件数据
 * @param {*} req
 * @param {*} res
 */
exports.addFiles = (req, res) => {}

/**
 * 查询文件列表
 * @param {*} req
 * @param {*} res
 */
exports.getFileLsit = (req, res) => {
  // 分页加载
  const page_num = req.query.page_num || 1 //当前的num
  const page_size = req.query.page_size || 10 //当前页的数量
  const file_name = req.query.file_name
  const params = [
    (parseInt(page_num) - 1) * parseInt(page_size),
    parseInt(page_size)
  ]

  //   let sort = "asc"; //asc：升序 desc：降序

  // 查询
  // const selectSql = `select * from sys_file limit ?,?`;
  const selectSql =
    ` select a.*, b.username user_name from sys_file as a
                    left join sys_user as b on a.user_id = b.id where a.is_delete = 0` +
    (file_name ? ` and a.file_name like '%${file_name}%'` : '') +
    ` limit ?,?`
  // 查询数据库总数
  const totalSql =
    `select count(*) as total from sys_file where is_delete = 0` +
    (file_name ? ` and file_name like '%${file_name}%'` : '')
  let pm_list = new Promise((resolve, reject) => {
    db.query(selectSql, params, (err, results) => {
      if (err) return reject(err)
      results.map((item) => {
        item.create_time = formatTime(item.create_time)
      })
      resolve(results)
    })
  })

  let pm_total = new Promise((resolve, reject) => {
    db.query(totalSql, (err, results) => {
      if (err) return reject(err)
      //   console.log(results, "results");
      resolve(results[0].total)
    })
  })

  Promise.all([pm_list, pm_total]).then((result) => {
    res.send({
      status: 0,
      message: '获取文件列表成功',
      total: result[1],
      data: result[0] || []
    })
  })
}

/**
 * 查询图标列表
 * @param {*} req
 * @param {*} res
 */
exports.getIconLsit = (req, res) => {
  // 查询
  const selectSql = `select * from sys_icon`

  db.query(selectSql, (err, results) => {
    if (err) return res.new_send(err)
    res.send({
      status: 0,
      message: '获取图标列表成功',
      data: results
    })
  })
}
