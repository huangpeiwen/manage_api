const multer = require('multer')
const path = require('path')
const db = require('../db')
const { nanoid } = require('nanoid')
const { getNowTime } = require('../utils')

// 限制图片上传类型
const fileFilter = (req, file, cb) => {
  let fileTypes = ['image/jpeg', 'image/png', 'image/jpg', 'image/gif']
  if (fileTypes.indexOf(file.mimetype) !== -1) {
    cb(null, true)
  } else {
    cb(null, false)
  }
}

// 保存头像到本地路径
const storage_avatar = multer.diskStorage({
  destination: path.resolve('public/avatar/'),
  filename: (req, file, cb) => {
    // 后缀名
    let extName = path.extname(file.originalname)
    // 文件名(无后缀名)
    let basename = path.basename(file.originalname, extName)
    // 新文件名
    let fileName = basename + '_' + nanoid() + extName
    cb(null, fileName)
  }
})

//   保存图片到本地路径
const storage_image = multer.diskStorage({
  destination: path.resolve('public/image/'),
  filename: (req, file, cb) => {
    // 后缀名
    let extName = path.extname(file.originalname)
    // 文件名(无后缀名)
    let basename = path.basename(file.originalname, extName)
    let fileName = basename + '_' + nanoid() + extName
    cb(null, fileName)
  }
})

//   保存文件到本地路径
const storage_file = multer.diskStorage({
  destination: path.resolve('public/file/'),
  filename: (req, file, cb) => {
    // 后缀名
    let extName = path.extname(file.originalname)
    // 文件名(无后缀名)
    let basename = path.basename(file.originalname, extName)
    // 新文件名
    let fileName = basename + '_' + nanoid() + extName
    cb(null, fileName)
  }
})

// 文件最大内存大小
const limits = {
  fileSize: '1000MB'
}

// 头像上传(单文件)
exports.upload_avatar = multer({
  fileFilter,
  storage: storage_avatar,
  limits
}).single('avatar')

// 图片上传(单文件)
exports.upload_image = multer({
  fileFilter,
  storage: storage_image,
  limits
}).single('image')

// 图片上传(多文件)
exports.upload_images = multer({
  fileFilter,
  storage: storage_image,
  limits
}).array('images', 1000)

// 文件上传(单文件)
exports.upload_file = multer({
  storage: storage_file,
  limits
}).single('file')

// 文件上传(多文件)
exports.upload_files = multer({
  storage: storage_file,
  limits
}).array('files', 1000)

// 图片上传(单文件)
exports.saveImage = (req, res) => {
  if (!req.file || req.file.fieldname !== 'image')
    return res.new_send('图片上传失败')

  // 图片上传成功处理
  const { filename } = req.file
  const file_url = '/image/' + filename
  res.send({
    status: 0,
    message: '图片上传成功',
    data: { file_url }
  })

  // const user_id = req.auth.id || "";
  // const { biz_id } = req.body;
  // const { filename, mimetype, size, originalname } = req.file;
  // const file_url = "/image/" + filename;
  // const file = {
  //   biz_id: biz_id, // 业务主表ID
  //   file_url: file_url, // 文件路径
  //   file_name: filename, // 文件名称
  //   file_type: mimetype, // 文件类型
  //   file_size: size, // 文件大小
  //   file_extname: path.extname(originalname), // 文件扩展名
  //   user_id: user_id, // 创建人Id
  //   create_time: getNowTime(), // 创建时间
  //   remark: "", // 备注
  // };
  // // 插入数据库
  // const insertSql = `insert into sys_file set ?`;
  // db.query(insertSql, file, (err, results) => {
  //   if (err) return res.new_send(err);
  //   if (results.affectedRows <= 0) return res.new_send("图片上传失败");
  //   res.send({
  //     status: 0,
  //     message: "图片上传成功",
  //     data: { insertId: results.insertId, file_url },
  //   });
  // });
}

// 图片上传(多文件)
exports.saveImages = async (req, res) => {
  if (req.files.length <= 0) return res.new_send('图片上传失败')

  // 图片上传成功处理
  let fileArr = []
  let resData = []
  req.files.map((item) => {
    let file_url = '/image/' + item.filename
    fileArr.push([
      req.body.biz_id, // biz_id
      file_url, // file_url
      item.filename, // file_name
      item.mimetype, // file_type
      item.size, // file_size
      path.extname(item.originalname), // file_extname
      req.auth.id || '', // user_id
      getNowTime(), // create_time
      '' // remark
    ])
    // 返回结果
    resData.push({
      id: null,
      file_url
    })
  })
  // 插入数据库
  const insertSql = `insert into sys_file (biz_id, file_url, file_name, file_type, file_size, file_extname, user_id, create_time, remark) values ?`
  db.query(insertSql, [fileArr], (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows <= 0) return res.new_send('图片上传失败')
    // 图片上传成功处理
    resData.map((item, index) => {
      item.id = results.insertId + index
    })
    res.send({
      status: 0,
      message: '图片上传成功',
      data: resData
    })
  })
}

// 文件上传(单文件)
exports.saveFile = (req, res) => {
  if (!req.file || req.file.fieldname !== 'file')
    return res.new_send('文件上传失败')

  // 图片上传成功处理
  const user_id = req.auth.id || ''
  const { biz_id } = req.body
  const { filename, mimetype, size, originalname } = req.file
  const file_url = '/file/' + filename
  const file = {
    biz_id: biz_id, // 业务主表ID
    file_url: file_url, // 文件路径
    file_name: filename, // 文件名称
    file_type: mimetype, // 文件类型
    file_size: size, // 文件大小
    file_extname: path.extname(originalname), // 文件扩展名
    user_id: user_id, // 创建人Id
    create_time: getNowTime(), // 创建时间
    remark: '' // 备注
  }
  // 插入数据库
  const insertSql = `insert into sys_file set ?`
  db.query(insertSql, file, (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows <= 0) return res.new_send('图片上传失败')
    // 图片上传成功处理
    res.send({
      status: 0,
      message: '图片上传成功',
      data: { insertId: results.insertId, file_url }
    })
  })
}

// 文件上传(多文件)
exports.saveFiles = (req, res) => {
  //   console.log(req, "req.files");
  if (req.files.length <= 0) return res.new_send('文件上传失败')

  // 图片上传成功处理
  let fileArr = []
  let resData = []
  req.files.map((item) => {
    let file_url = '/file/' + item.filename
    fileArr.push([
      req.body.biz_id, // biz_id
      file_url, // file_url
      item.filename, // file_name
      item.mimetype, // file_type
      item.size, // file_size
      path.extname(item.originalname), // file_extname
      req.auth.id || '', // user_id
      getNowTime(), // create_time
      '' // remark
    ])
    // 返回结果
    resData.push({
      id: null,
      file_url
    })
  })
  // 插入数据库
  const insertSql = `insert into sys_file (biz_id, file_url, file_name, file_type, file_size, file_extname, user_id, create_time, remark) values ?`
  db.query(insertSql, [fileArr], (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows <= 0) return res.new_send('文件上传失败')
    // 文件上传成功处理
    resData.map((item, index) => {
      item.id = results.insertId + index
    })
    res.send({
      status: 0,
      message: '文件上传成功',
      data: resData
    })
  })
}
