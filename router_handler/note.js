// 博客笔记 blog_note
const db = require('../db')
const { getNowTime, formatTime } = require('../utils/index')

/**
 * 新增博客笔记
 * @param {*} req
 * @param {*} res
 */
exports.addNote = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 插入数据库
  const insertSql = `insert into blog_note set ?`
  db.query(insertSql, req.body, (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows !== 1) return res.new_send('新增博客笔记失败')
    res.send({
      status: 0,
      message: '新增博客笔记成功',
      data: { insertId: results.insertId }
    })
  })
}

/**
 * 修改博客笔记
 * @param {*} req
 * @param {*} res
 */
exports.updateNote = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 修改数据库
  const updateSql = `update blog_note set ? where id = ?`
  db.query(updateSql, [req.body, req.body.id], (err, results) => {
    console.log(err, results)
    if (err) return res.new_send(err)
    if (results.affectedRows !== 1) return res.new_send('更新博客笔记失败')
    res.new_send('更新博客笔记成功', 0)
  })
}

/**
 * 删除博客笔记
 * @param {*} req
 * @param {*} res
 */
exports.deleteNoteById = (req, res) => {
  const deleteSql = `update blog_note set is_delete = 1 where id = ?`
  db.query(deleteSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows <= 0) return res.new_send('删除博客笔记失败')
    res.new_send('删除博客笔记成功', 0)
  })
}

/**
 * 获取笔记列表
 * @param {*} req
 * @param {*} res
 */
exports.getNoteLsit = (req, res) => {
  // 分页加载
  const page_num = req.query.page_num || 1 //当前页
  const page_size = req.query.page_size || 10 //当前页的数量
  const name = req.query.name
  const params = [
    (parseInt(page_num) - 1) * parseInt(page_size),
    parseInt(page_size)
  ]
  // 查询
  const selectSql =
    `select a.*, 
    (
        select GROUP_CONCAT(ct.name) as names from sys_catalog as ct where CONCAT(a.catalog_ids, "," ) like CONCAT('%', ct.id, ",", '%') 
    ) as catalog_names,
    (
        select GROUP_CONCAT(st.name) as names from sys_sort as st where CONCAT(a.sort_ids, "," ) like CONCAT('%', st.id, ",", '%') 
    ) as sort_names,
    (
        select GROUP_CONCAT(cg.name) as names from sys_tag as cg where CONCAT(a.sort_ids, "," ) like CONCAT('%', cg.id, ",", '%') 
    ) as tag_names, b.username user_name
    from blog_note as a left join sys_user as b on a.user_id = b.id where a.is_delete = 0` +
    (name ? ` and a.name like '%${name}%'` : '') +
    ` limit ?,?`
  db.query(selectSql, params, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
      })
    }
    // 列表数据
    let notes = results

    // 查询数据库总数
    const totalSql =
      'select count(*) as total from blog_note where is_delete = 0' +
      (name ? ` and name like '%${name}%'` : '')
    db.query(totalSql, (err, results) => {
      if (err) return res.new_send(err)
      let total = results[0]['total'] // 总数
      res.send({
        status: 0,
        message: '获取博客笔记列表成功',
        page_num,
        page_size,
        total,
        data: notes
      })
    })
  })
}

/**
 * 获取博客笔记
 * @param {*} req
 * @param {*} res
 */
exports.getNoteById = (req, res) => {
  const selectSql = `select a.*, 
  (
      select GROUP_CONCAT(ct.name) as names from sys_catalog as ct where CONCAT(a.catalog_ids, "," ) like CONCAT('%', ct.id, ",", '%') 
  ) as catalog_names,
  (
      select GROUP_CONCAT(st.name) as names from sys_sort as st where CONCAT(a.sort_ids, "," ) like CONCAT('%', st.id, ",", '%') 
  ) as sort_names,
  (
      select GROUP_CONCAT(cg.name) as names from sys_tag as cg where CONCAT(a.sort_ids, "," ) like CONCAT('%', cg.id, ",", '%') 
  ) as tag_names, b.username user_name
  from blog_note as a left join sys_user as b on a.user_id = b.id where a.id = ?`
  db.query(selectSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
      })
    }
    res.send({
      status: 0,
      message: '获取博客笔记成功',
      data: results
    })
  })
}

/**
 * 新增博客笔记从表
 * @param {*} req
 * @param {*} res
 */
exports.addNoteUrl = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 插入数据库
  const insertSql = `insert into blog_note_url set ?`
  db.query(insertSql, req.body, (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows !== 1) return res.new_send('新增博客笔记从表失败')
    res.send({
      status: 0,
      message: '新增博客笔记从表成功',
      data: { insertId: results.insertId }
    })
  })
}

/**
 * 修改博客笔记从表
 * @param {*} req
 * @param {*} res
 */
exports.updateNoteUrl = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 修改数据库
  const updateSql = `update blog_note_url set ? where id = ?`
  db.query(updateSql, [req.body, req.body.id], (err, results) => {
    console.log(err, results)
    if (err) return res.new_send(err)
    if (results.affectedRows !== 1) return res.new_send('更新博客笔记从表失败')
    res.new_send('更新博客笔记从表成功', 0)
  })
}

/**
 * 删除博客笔记从表
 * @param {*} req
 * @param {*} res
 */
exports.deleteNoteUrlById = (req, res) => {
  const deleteSql = `update blog_note_url set is_delete = 1 where id = ?`
  db.query(deleteSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows <= 0) return res.new_send('删除博客笔记从表失败')
    res.new_send('删除博客笔记从表成功', 0)
  })
}

/**
 * 获取博客笔记从表列表
 * @param {*} req
 * @param {*} res
 */
exports.getNoteUrlLsit = (req, res) => {
  const note_id = req.query.note_id
  const selectSql =
    `select * from blog_note_url where is_delete = 0` +
    (note_id ? ` and note_id = ?` : '')
  db.query(selectSql, note_id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
      })
    }
    res.send({
      status: 0,
      message: '获取博客笔记从表列表成功',
      data: results
    })
  })
}

/**
 * 获取博客笔记从表
 * @param {*} req
 * @param {*} res
 */
exports.getNoteUrlById = (req, res) => {
  const selectSql = `select * from blog_note_url where id = ?`
  db.query(selectSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
      })
    }
    res.send({
      status: 0,
      message: '获取博客笔记从表成功',
      data: results
    })
  })
}
