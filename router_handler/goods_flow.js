// 已购流水 buy_goods_flow
const db = require('../db')
const { getNowTime, formatTime } = require('../utils/index')

/**
 * 新增已购流水
 * @param {*} req
 * @param {*} res
 */
exports.addGoodsFlow = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 插入数据库
  const insertSql = `insert into buy_goods_flow set ?`
  db.query(insertSql, req.body, (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows !== 1) return res.new_send('新增已购流水失败')
    res.send({
      status: 0,
      message: '新增已购流水成功',
      data: { insertId: results.insertId }
    })
  })
}

/**
 * 修改已购流水
 * @param {*} req
 * @param {*} res
 */
exports.updateGoodsFlow = (req, res) => {
  // 日期时间格式化
  req.body.create_time = req.body.create_time
    ? formatTime(req.body.create_time)
    : getNowTime()
  // 修改数据库
  const updateSql = `update buy_goods_flow set ? where id = ?`
  db.query(updateSql, [req.body, req.body.id], (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows !== 1) return res.new_send('更新已购流水失败')
    res.new_send('更新已购流水成功', 0)
  })
}

/**
 * 删除已购流水
 * @param {*} req
 * @param {*} res
 */
exports.deleteGoodsFlowById = (req, res) => {
  const deleteSql = `update buy_goods_flow set is_delete = 1 where id = ?`
  db.query(deleteSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.affectedRows <= 0) return res.new_send('删除已购流水失败')
    res.new_send('删除已购流水成功', 0)
  })
}

/**
 * 获取已购流水列表
 * @param {*} req
 * @param {*} res
 */
exports.getGoodsFlowLsit = (req, res) => {
  // 分页加载
  const page_num = req.query.page_num || 1 //当前页
  const page_size = req.query.page_size || 10 //当前页的数量
  const good_name = req.query.good_name
  const params = [
    (parseInt(page_num) - 1) * parseInt(page_size),
    parseInt(page_size)
  ]
  // 查询
  const selectSql =
    `select b.*, a.id as flowId, a.buy_price, a.buy_time, a.create_time as flow_time, c.username user_name  from buy_goods_flow as a
  left join buy_guide as b on a.guide_id = b.id
  left join sys_user as c on b.user_id = c.id
  where a.is_delete = 0` +
    (good_name ? ` and b.good_name like '%${good_name}%'` : '') +
    ` limit ?,?`
  db.query(selectSql, params, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
        item.flow_time = formatTime(item.flow_time)
      })
    }
    // 列表数据
    let flows = results

    // 查询数据库总数
    const totalSql =
      'select count(*) as total from buy_goods_flow as a left join buy_guide as b on a.guide_id = b.id where a.is_delete = 0' +
      (good_name ? ` and b.good_name like '%${good_name}%'` : '')
    db.query(totalSql, (err, results) => {
      if (err) return res.new_send(err)
      let total = results[0]['total'] // 总数
      res.send({
        status: 0,
        message: '获取已购流水列表成功',
        page_num,
        page_size,
        total,
        data: flows
      })
    })
  })
}

/**
 * 获取已购流水
 * @param {*} req
 * @param {*} res
 */
exports.getGoodsFlowById = (req, res) => {
  const selectSql = `select b.*, a.id as flowId, a.buy_price, a.buy_time, a.create_time as flow_time, c.username user_name from buy_goods_flow as a
  left join buy_guide as b on a.guide_id = b.id
  left join sys_user as c on b.user_id = c.id
  where a.id = ?`
  db.query(selectSql, req.params.id, (err, results) => {
    if (err) return res.new_send(err)
    if (results.length > 0) {
      results.forEach((item) => {
        item.create_time = formatTime(item.create_time)
        item.flow_time = formatTime(item.flow_time)
      })
    }
    res.send({
      status: 0,
      message: '获取已购流水成功',
      data: results
    })
  })
}
