// 获取当前日期时间
const { getNowTime } = require('../utils')
// 密码加密
const bcrypt = require('bcryptjs')
// 随机ID
const { nanoid } = require('nanoid')
// 集成log4js日志
const log4js = require('../utils/log4js')
const logger = log4js.getLogger('runtime')

/**
 * 初始化用户
 * @param {*} db
 */
exports.initUser = (db) => {
  let selectSql = `select count(*) num from sys_user`
  db.query(selectSql, (err, results) => {
    if (err) return res.new_send(err)
    if (results[0].num <= 0) {
      let user = {
        id: 1,
        username: 'admin',
        password: bcrypt.hashSync('888', 10),
        nick_name: '用户' + nanoid(5),
        sex: '男',
        email: '1042850644@qq.com',
        mobile: null,
        avatar: '/default/1.jpg',
        create_time: getNowTime(),
        status: true,
        remark: '初始化用户',
        is_admin: true,
        is_delete: false
      }
      const insertSql = `insert into sys_user set ?`
      db.query(insertSql, user, (err, results) => {
        if (results.affectedRows > 0) logger.info('用户初始化成功...')
      })
    } else {
      // 初始化用户已存在...
    }
  })
}

/**
 * 初始化后台菜单
 * @param {*} db
 */
// exports.initMenuAfter = (db) => {
//   let selectSql = `select count(*) num from sys_menu`
//   db.query(selectSql, (err, results) => {
//     if (results[0].num <= 0) {
//       let menus = [
//         [
//           1, // id：主键ID
//           0, // parent_id：父菜单ID
//           'home', // icon：菜单图标
//           'Home', // code：菜单编码
//           '首页', // name：菜单名称
//           '/admin', // path: 菜单路径(如:/sys/user)
//           1, // order_num：排序
//           'pc', // type：类型
//           false, // is_operate：是否编辑权限
//           0, // catalog_id：目录ID(二级菜单)
//           getNowTime(), // create_time: 创建时间
//           '系统内置菜单不可操作', // remark: 备注
//           false // is_delete: 是否删除
//         ],
//         [
//           2, // id：主键ID
//           0, // parent_id：父菜单ID
//           'user', // icon：菜单图标
//           'UserManage', // code：菜单编码
//           '用户管理', // name：菜单名称
//           '/user', // path: 菜单路径(如:/sys/user)
//           90, // order_num：排序
//           'pc', // type：类型
//           false, // is_operate：是否编辑权限
//           0, // catalog_id：目录ID(二级菜单)
//           getNowTime(), // create_time: 创建时间
//           '系统内置菜单不可操作', // remark: 备注
//           false // is_delete: 是否删除
//         ],
//         [
//           3, // id：主键ID
//           2, // parent_id：父菜单ID
//           'user-list', // icon：菜单图标
//           'User', // code：菜单编码
//           '用户列表', // name：菜单名称
//           '/admin/user/list', // path: 菜单路径(如:/sys/user)
//           901, // order_num：排序
//           'pc', // type：类型
//           false, // is_operate：是否编辑权限
//           0, // catalog_id：目录ID(二级菜单)
//           getNowTime(), // create_time: 创建时间
//           '系统内置菜单不可操作', // remark: 备注
//           false // is_delete: 是否删除
//         ],
//         [
//           4, // id：主键ID
//           0, // parent_id：父菜单ID
//           'setting', // icon：菜单图标
//           'SystemManage', // code：菜单编码
//           '系统管理', // name：菜单名称
//           '/system', // path: 菜单路径(如:/sys/user)
//           91, // order_num：排序
//           'pc', // type：类型
//           false, // is_operate：是否编辑权限
//           0, // catalog_id：目录ID(二级菜单)
//           getNowTime(), // create_time: 创建时间
//           '系统内置菜单不可操作', // remark: 备注
//           false // is_delete: 是否删除
//         ],
//         [
//           5, // id：主键ID
//           4, // parent_id：父菜单ID
//           'menu', // icon：菜单图标
//           'Menu', // code：菜单编码
//           '菜单管理', // name：菜单名称
//           '/admin/system/menu', // path: 菜单路径(如:/sys/user)
//           911, // order_num：排序
//           'pc', // type：类型
//           false, // is_operate：是否编辑权限
//           0, // catalog_id：目录ID(二级菜单)
//           getNowTime(), // create_time: 创建时间
//           '系统内置菜单不可操作', // remark: 备注
//           false // is_delete: 是否删除
//         ],
//         [
//           6, // id：主键ID
//           4, // parent_id：父菜单ID
//           'catalog', // icon：菜单图标
//           'Catalog', // code：菜单编码
//           '目录管理', // name：菜单名称
//           '/admin/system/catalog', // path: 菜单路径(如:/sys/user)
//           912, // order_num：排序
//           'pc', // type：类型
//           false, // is_operate：是否编辑权限
//           0, // catalog_id：目录ID(二级菜单)
//           getNowTime(), // create_time: 创建时间
//           '系统内置菜单不可操作', // remark: 备注
//           false // is_delete: 是否删除
//         ],
//         [
//           7, // id：主键ID
//           4, // parent_id：父菜单ID
//           'sort', // icon：菜单图标
//           'CatalogSort', // code：菜单编码
//           '分类管理', // name：菜单名称
//           '/admin/system/sort', // path: 菜单路径(如:/sys/user)
//           913, // order_num：排序
//           'pc', // type：类型
//           false, // is_operate：是否编辑权限
//           0, // catalog_id：目录ID(二级菜单)
//           getNowTime(), // create_time: 创建时间
//           '系统内置菜单不可操作', // remark: 备注
//           false // is_delete: 是否删除
//         ],
//         [
//           8, // id：主键ID
//           4, // parent_id：父菜单ID
//           'tag', // icon：菜单图标
//           'Tag', // code：菜单编码
//           '标签管理', // name：菜单名称
//           '/admin/system/tag', // path: 菜单路径(如:/sys/user)
//           914, // order_num：排序
//           'pc', // type：类型
//           false, // is_operate：是否编辑权限
//           0, // catalog_id：目录ID(二级菜单)
//           getNowTime(), // create_time: 创建时间
//           '系统内置菜单不可操作', // remark: 备注
//           false // is_delete: 是否删除
//         ],
//         [
//           9, // id：主键ID
//           4, // parent_id：父菜单ID
//           'field', // icon：菜单图标
//           'Field', // code：菜单编码
//           '字段库', // name：菜单名称
//           '/admin/system/field', // path: 菜单路径(如:/sys/user)
//           915, // order_num：排序
//           'pc', // type：类型
//           false, // is_operate：是否编辑权限
//           0, // catalog_id：目录ID(二级菜单)
//           getNowTime(), // create_time: 创建时间
//           '系统内置菜单不可操作', // remark: 备注
//           false // is_delete: 是否删除
//         ],
//         [
//           10, // id：主键ID
//           4, // parent_id：父菜单ID
//           'dictionary', // icon：菜单图标
//           'Dictionary', // code：菜单编码
//           '字典库', // name：菜单名称
//           '/admin/system/dictionary', // path: 菜单路径(如:/sys/user)
//           916, // order_num：排序
//           'pc', // type：类型
//           false, // is_operate：是否编辑权限
//           0, // catalog_id：目录ID(二级菜单)
//           getNowTime(), // create_time: 创建时间
//           '系统内置菜单不可操作', // remark: 备注
//           false // is_delete: 是否删除
//         ],
//         [
//           11, // id：主键ID
//           4, // parent_id：父菜单ID
//           'file', // icon：菜单图标
//           'File', // code：菜单编码
//           '文件库', // name：菜单名称
//           '/admin/system/file', // path: 菜单路径(如:/sys/user)
//           917, // order_num：排序
//           'pc', // type：类型
//           false, // is_operate：是否编辑权限
//           0, // catalog_id：目录ID(二级菜单)
//           getNowTime(), // create_time: 创建时间
//           '系统内置菜单不可操作', // remark: 备注
//           false // is_delete: 是否删除
//         ],
//         [
//           12, // id：主键ID
//           4, // parent_id：父菜单ID
//           'login-log', // icon：菜单图标
//           'LoginLog', // code：菜单编码
//           '登录日志', // name：菜单名称
//           '/admin/system/login_log', // path: 菜单路径(如:/sys/user)
//           918, // order_num：排序
//           'pc', // type：类型
//           false, // is_operate：是否编辑权限
//           0, // catalog_id：目录ID(二级菜单)
//           getNowTime(), // create_time: 创建时间
//           '系统内置菜单不可操作', // remark: 备注
//           false // is_delete: 是否删除
//         ],
//         [
//           13, // id：主键ID
//           4, // parent_id：父菜单ID
//           'operation-log', // icon：菜单图标
//           'OperationLog', // code：菜单编码
//           '操作日志', // name：菜单名称
//           '/admin/system/operation_log', // path: 菜单路径(如:/sys/user)
//           919, // order_num：排序
//           'pc', // type：类型
//           false, // is_operate：是否编辑权限
//           0, // catalog_id：目录ID(二级菜单)
//           getNowTime(), // create_time: 创建时间
//           '系统内置菜单不可操作', // remark: 备注
//           false // is_delete: 是否删除
//         ]
//       ]

//       const insertSql = `insert into sys_menu (id, parent_id, icon, code, name, path, order_num, type, is_operate, catalog_id, create_time, remark, is_delete) values ?`
//       db.query(insertSql, [menus], (err, results) => {
//         if (results.affectedRows > 0) logger.info('后台菜单初始化成功...')
//       })
//     } else {
//       // 初始化后台菜单已存在...
//     }
//   })
// }
