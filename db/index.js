const mysql = require('mysql')
const { initUser, initMenuAfter } = require('./init')
// 集成log4js日志
const log4js = require('../utils/log4js')
const logger = log4js.getLogger('runtime')

// 数据库
const db = mysql.createPool({
  host: '120.79.114.184',
  user: 'root',
  password: '123456',
  database: 'manage_db'
})

// 连接数据库
db.getConnection((err, connection) => {
  if (err) {
    logger.error('数据库连接失败...', err)
  } else {
    logger.info('数据库连接成功...')
    // 初始化用户
    initUser(db)
    // 初始化后台菜单
    // initMenuAfter(db);
  }
})

module.exports = db
