

# MANAGE_API数据库表

| 表名                | 表中文名       | 接口 |
| ------------------- | -------------- | ---- |
| sys_user            | 用户管理表     | √    |
| sys_tag             | 标签管理表     | √ √  |
| sys_sort            | 分类管理表     | √ √  |
| sys_sort_tag        | 分类关联标签表 | √ √  |
| sys_catalog         | 目录管理表     | √ √  |
| sys_catalog_sort    | 目录关联分类表 | √ √  |
| sys_field           | 字段库表       | √ √  |
| sys_dictionary      | 字典库主表     | √ √  |
| sys_dictionary_data | 字典库从表     | √ √  |
| sys_file            | 文件管理表     | √ √  |
| sys_icon            | 图标库表       | √ √  |
| sys_login_log       | 登录日志表     | √ √  |
| sys_operation_log   | 操作日志表     | √ √  |
| blog_website        | 网站导航表     | √ √  |
| blog_note           | 博客笔记表     | √ √  |
| blog_note_url       | 博客笔记从表   | √ √  |
| blog_code           | 代码片段表     | √ √  |
| blog_code_url       | 代码片段从表   | √ √  |
| blog_project        | 项目源码表     | √ √  |
| blog_software       | 软件管理表     | √ √  |
| blog_book           | 书籍推荐表     | √ √  |
| buy_guide           | 商品导购表     | √ √  |
| buy_goods_flow      | 已购流水表     | √ √  |
| buy_goods_pricing   | 未购计价表     | √ √  |
|                     |                |      |
|                     |                |      |
|                     |                |      |



## 系统管理

### 用户管理表(sys_user)

```sql
CREATE TABLE `sys_user` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `username` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户名',
  `password` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '密码',
  `nick_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '昵称',
  `sex` tinyint(1) DEFAULT NULL COMMENT '性别(1:男 2:女)',
  `email` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '手机号',
  `avatar` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '头像',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态(0:禁用 1:正常)',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注(用户说明)',
  `is_admin` tinyint(1) DEFAULT NULL COMMENT '是否管理员',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '是否删除(1:已删除 0:正常)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户管理表';
```

![sys_user](https://p.sda1.dev/13/b71860d5bb03e0cc145242627b61f222/Snipaste_2023-09-03_13-45-57.png)



### 菜单管理表(sys_menu)【去除】

```sql
CREATE TABLE `sys_menu` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `parent_id` int DEFAULT NULL COMMENT '父菜单ID',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '菜单图标',
  `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '菜单编码',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '菜单名称',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '菜单路径(如:/sys/user)',
  `perms` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '授权(sys:user:add,sys:user:edit)',
  `type` int DEFAULT NULL COMMENT '类型(0:目录 1:菜单 2:链接)',
  `order_num` int DEFAULT NULL COMMENT '排序',
  `is_blog` tinyint DEFAULT NULL COMMENT '是否博客端',
  `is_app` tinyint DEFAULT NULL COMMENT '是否移动端',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注(菜单说明)',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '是否删除(1:已删除 0:正常)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='菜单管理表';
```

![sys_menu](https://p.sda1.dev/13/6ce4aa5785c0f3ee3ba6b8d73da1bb8d/Snipaste_2023-09-03_14-17-42.png)



### 目录管理表(sys_catalog)

```sql
CREATE TABLE `sys_catalog` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '目录图标',
  `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '目录编码',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '目录名称',
  `user_id` bigint DEFAULT NULL COMMENT '创建人Id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注(目录说明)',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '是否删除(1:已删除 0:正常)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='目录管理表';
```

![sys_catalog](https://p.sda1.dev/13/aab6e208d91fbd3ab197e375e9944ffe/Snipaste_2023-09-03_14-46-25.png)



### 菜单关联目录表(sys_menu_catalog)【去除】

```sql
CREATE TABLE `sys_menu_catalog` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `menu_id` int DEFAULT NULL COMMENT '菜单ID',
  `catalog_id` int DEFAULT NULL COMMENT '目录ID',
  `user_id` bigint DEFAULT NULL COMMENT '创建人ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注(分类说明)',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '是否删除(1:已删除 0:正常)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='分类管理表';
```

![sys_menu_catalog](https://p.sda1.dev/13/c0c31caaa8f5edca8071a924bdf5bbe4/image-20230903145050542.png)



### 分类管理表(sys_sort)

```sql
CREATE TABLE `sys_sort` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '分类图标',
  `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '分类编码',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '分类名称',
  `user_id` bigint DEFAULT NULL COMMENT '创建人ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注(分类说明)',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '是否删除(1:已删除 0:正常)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='分类管理表';
```

![sys_sort](https://p.sda1.dev/13/dba1ceee9f94a6b3bd4dfc014698b141/Snipaste_2023-09-03_14-58-18.png)



### 目录关联分类表(sys_catalog_sort)

```sql
CREATE TABLE `sys_catalog_sort` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `catalog_id` int DEFAULT NULL COMMENT '目录ID',
  `sort_id` int DEFAULT NULL COMMENT '分类ID',
  `user_id` bigint DEFAULT NULL COMMENT '创建人ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注(分类说明)',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '是否删除(1:已删除 0:正常)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='分类管理表';
```

![sys_catalog_sort](https://p.sda1.dev/13/2e86fb6735583837dc429ab01958f0ab/Snipaste_2023-09-03_15-01-29.png)



### 字段库表(sys_field)

```sql
CREATE TABLE `sys_field` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `code` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '字段编码',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '字段名称',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注(字段说明)',
  `user_id` int DEFAULT NULL COMMENT '创建人Id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '是否删除(1:已删除 0:正常)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='字段库表';
```

![sys_field](https://p.sda1.dev/13/7cb91e15e5e43d594fac1ab3587b4c45/Snipaste_2023-09-03_18-21-44.png)



### 字典库(sys_dictionary)

```sql
CREATE TABLE `sys_dictionary` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '字典编码',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '字典名称',
  `user_id` bigint DEFAULT NULL COMMENT '创建人ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '是否删除(1:已删除 0:正常)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='字典库表';
```

![sys_dictionary](https://p.sda1.dev/13/0b987a8ce990501a46a6796ca194c691/Snipaste_2023-09-03_18-25-28.png)



### 字典库数据(sys_dictionary_data)

```sql
CREATE TABLE `sys_dictionary_data` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `dictionary_id` bigint DEFAULT NULL COMMENT '字典ID',
  `data_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '数据名',
  `data_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '数据值',
  `user_id` bigint DEFAULT NULL COMMENT '创建人ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注(数据说明)',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '是否删除(1:已删除 0:正常)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='字典库数据';
```

![sys_dictionary_data](https://p.sda1.dev/13/d53d60722498b3996ddb89814b8bd1b1/Snipaste_2023-09-03_18-30-10.png)



### 文件管理表(sys_file)

```sql
CREATE TABLE `sys_file` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `biz_id` int DEFAULT NULL COMMENT '业务主表ID',
  `file_url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '文件路径',
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '文件名称',
  `file_type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '文件类型',
  `file_size` double DEFAULT NULL COMMENT '文件大小',
  `file_extname` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '文件扩展名',
  `user_id` bigint DEFAULT NULL COMMENT '创建人Id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='文件管理表';
```

![sys_file](https://p.sda1.dev/13/1eef02656f0aa306ca623665ef8e123f/Snipaste_2023-09-03_18-32-02.png)



### 图标库表(sys_icon)

```sql
CREATE TABLE `sys_icon` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(64) DEFAULT NULL COMMENT '编码',
  `name` varchar(64) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

![sys_icon](https://p.sda1.dev/13/adb7d634fca156ff1175d3dff60b2811/Snipaste_2023-09-03_18-34-03.png)



### 登录日志表(sys_login_log)

```sql
CREATE TABLE `sys_login_log` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `username` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '用户名',
  `status` varchar(32) DEFAULT NULL COMMENT '登录状态(online：在线，logout：退出登录)',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `create_by` int DEFAULT NULL COMMENT '创建人ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_update_by` int DEFAULT NULL COMMENT '更新人ID',
  `last_update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='登录日志表';
```

![sys_login_log](https://p.sda1.dev/13/917acb1bc3bd4da3a3f8c21c63161094/Snipaste_2023-09-03_18-36-30.png)



### 操作日志表(sys_operation_log)

```sql
CREATE TABLE `sys_operation_log` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `username` varchar(32) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '用户操作',
  `method` varchar(255) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(10000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '请求参数',
  `time` int DEFAULT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `create_by` int DEFAULT NULL COMMENT '创建人ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_update_by` int DEFAULT NULL COMMENT '更新人ID',
  `last_update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

![sys_operation_log](https://p.sda1.dev/13/0600b5949a810520f5171581a5442066/Snipaste_2023-09-03_18-37-49.png)



## 博客导航

### 网站导航表(blog_website)

```sql
CREATE TABLE `blog_website` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID ',
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '网站logo',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '网站名称',
  `url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '网站地址',
  `gitee_url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'gitee地址',
  `github_url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'github地址',
  `reple_url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '补充地址',
  `type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '网站类型(pc，mobile，all)',
  `menu_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '菜单IDS',
  `catalog_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '目录IDS',
  `sort_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '分类IDS',
  `tag_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '标签IDS',
  `is_download` tinyint(1) DEFAULT NULL COMMENT '是否支持下载',
  `account` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账号',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '密码',
  `is_vip` tinyint(1) DEFAULT NULL COMMENT '是否会员',
  `vip_start` date DEFAULT NULL COMMENT '会员开始日期',
  `vip_end` date DEFAULT NULL COMMENT '会员结束日期',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `user_id` bigint DEFAULT NULL COMMENT '创建人Id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '是否删除(1:已删除 0:正常)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='网站导航表';
```

![blog_website](https://p.sda1.dev/13/05b6aeda159f3faea113655b36161223/Snipaste_2023-09-05_23-02-40.png)

### 博客笔记表(blog_note)

```sql
CREATE TABLE `blog_note` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `cover` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '笔记封面',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '笔记名称',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '笔记内容',
  `menu_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '菜单IDS',
  `catalog_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '目录IDS',
  `sort_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '分类IDS',
  `tag_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '标签IDS',
  `blog_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '原文链接1',
  `author` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '原作者',
  `blog_url2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '原文链接2',
  `author2` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '原作者2',
  `blog_url3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '原文链接3',
  `author3` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '原作者3',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `user_id` bigint DEFAULT NULL COMMENT '创建人ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '是否删除(1:已删除 0:正常)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='博客笔记';
```

![blog_note](https://p.sda1.dev/13/46a160d929699a2b21f5fdda71e89d44/Snipaste_2023-09-05_22-11-19.png)



### 项目源码表(blog_project)

```sql
CREATE TABLE `blog_project` (
  `id` int NOT NULL COMMENT '主键Id',
  `cover` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '源码封面',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '源码名称',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '源码内容',
  `web_url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '预览地址',
  `gitee_url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'gitee地址',
  `github_url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'github地址',
  `reple_url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '补充地址',
  `language` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '(中文、英文、其他)',
  `menu_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '菜单IDS',
  `catalog_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '目录IDS',
  `sort_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '分类IDS',
  `tag_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '标签IDS',
  `account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账号',
  `password` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '密码',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '源码说明',
  `user_id` bigint DEFAULT NULL COMMENT '创建人ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '是否删除(1:已删除 0:正常)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='项目源码表';
```

![blog_project](https://p.sda1.dev/13/fd118f2f5d8042e531af76c849a143e2/Snipaste_2023-09-05_23-08-36.png)



### 代码片段(blog_code)

```sql
CREATE TABLE `blog_code` (
  `id` int NOT NULL COMMENT '主键Id',
  `cover` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '源码封面',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '源码名称',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '源码内容',
  `code_url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '代码地址',
  `gitee_url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'gitee地址',
  `github_url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'github地址',
  `reple_url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '补充地址',
  `menu_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '菜单IDS',
  `catalog_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '目录IDS',
  `sort_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '分类IDS',
  `tag_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '标签IDS',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '源码说明',
  `user_id` bigint DEFAULT NULL COMMENT '创建人Id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '是否删除(1:已删除 0:正常)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='代码片段表';
```

![blog_code](https://p.sda1.dev/13/df0342591440e7f3fa53058ea4f5b9f3/Snipaste_2023-09-05_23-12-26.png)



### 编程软件(blog_software)

```sql
CREATE TABLE `blog_software` (
  `id` int NOT NULL COMMENT '主键ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '软件名称',
  `version` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '软件版本',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '安装教程',
  `type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '软件分类(应用软件、软件开发等)',
  `system` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '操作系统',
  `software_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '软件下载地址',
  `ali_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '阿里云盘下载地址',
  `ali_pwd` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '阿里云盘下载地址密钥',
  `baidu_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '百度云盘下载地址',
  `baidu_pwd` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '百度云盘下载地址密钥',
  `other_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '其他云盘下载地址',
  `other_pwd` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '其他云盘下载地址密钥',
  `is_free` tinyint(1) DEFAULT NULL COMMENT '是否免费',
  `vip_account` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '会员账号',
  `vip_password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '会员密码',
  `secret_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '授权密钥',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '软件说明',
  `user_id` bigint DEFAULT NULL COMMENT '创建人Id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '是否删除(1:已删除 0:正常)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='开发软件表';
```

![blog_software](https://p.sda1.dev/13/ef91698854f71a7e5ea37070e23ec726/Snipaste_2023-09-05_22-02-02.png)



### 书籍推荐(blog_book)





## 购物比价

### 商品导购()



### 购物软件()



### 购物网站()



### 已购流水()



### 未购计价()