const express = require("express");
const router = express.Router();
const expressJoi = require("@escook/express-joi");
// 校验数据
const { add_menu_schema, update_menu_schema } = require("../schema/menu")
const { isAdmin } = require("../utils/auth");
const { 
    addMenu, 
    updateMenu, updateMenu_isMobile, updateMenu_isPc, updateMenu_isAdmin, deleteMenuById,
    getMenuLsit, getMenuById, getMenuParent, getMenuChild, getMenuTree

} = require("../router_handler/menu")

// 新增菜单
router.post("/menu/add", expressJoi(add_menu_schema), isAdmin, addMenu)

// 修改菜单
router.put("/menu/update", expressJoi(update_menu_schema), isAdmin, updateMenu)

// 修改菜单_是否移动端
router.patch("/menu/update/is_mobile", isAdmin, updateMenu_isMobile)

// 修改菜单_是否电脑端
router.patch("/menu/update/is_pc", isAdmin, updateMenu_isPc)

// 修改菜单_是否电脑端
router.patch("/menu/update/is_admin", isAdmin, updateMenu_isAdmin)

// 删除菜单
router.delete("/menu/:id", isAdmin, deleteMenuById);

// 获取菜单列表
router.get("/menu/list", getMenuLsit);

// 获取菜单信息
router.get("/menu/:id", getMenuById);

// 获取菜单父级列表
router.get("/menu/list/parent", getMenuParent);

// 获取菜单子级列表
router.get("/menu/list/child", getMenuChild);

// 获取菜单树形列表
router.get("/menu/list/tree", getMenuTree);

module.exports = router;
