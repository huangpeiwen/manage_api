// 导入 joi 模块
const joi = require('joi')

// 主键Id
const id = joi.number().integer().required().messages({
  'number.base': '菜单ID类型错误',
  'any.required': '菜单ID必填'
})

// 父菜单ID
const parent_id = joi.number().integer().required().messages({
  'number.base': '父菜单ID类型错误',
  'any.required': '父菜单ID必填'
})

//  菜单编码
const code = joi.string().min(1).max(64).required().messages({
  'any.required': '菜单编码必填',
  'string.max': '菜单编码长度错误(64)'
})

//  菜单名称
const name = joi.string().min(1).max(128).required().messages({
  'any.required': '菜单名称必填',
  'string.max': '菜单名称长度错误(128)'
})

//   菜单路径
const path = joi.string().allow(null, '')

// 类型
const type = joi.string().min(1).required().messages({
  'any.required': '类型必填'
})

//   是否编辑权限
const is_operate = joi.number().integer().default(1)

//   菜单图标
const icon = joi.string().allow(null, '')

// 排序
const order_num = joi.number().integer().required().messages({
  'number.base': '排序类型错误',
  'any.required': '排序必填'
})

// 目录Id
const catalog_id = joi.number().integer().default(0)

// 创建时间
const create_time = joi.date() //.format("YYYY-MM-DD HH:mm:ss");

// 备注
const remark = joi.string().allow(null, '')

// 是否删除
const is_delete = joi.number().integer().default(0)

// 新增验证规则
exports.add_menu_schema = {
  body: {
    parent_id,
    icon,
    code,
    name,
    path,
    order_num,
    type,
    is_operate,
    catalog_id,
    create_time,
    remark,
    is_delete
  }
}

// 更新验证规则
exports.update_menu_schema = {
  body: {
    id,
    parent_id,
    icon,
    code,
    name,
    path,
    order_num,
    type,
    is_operate,
    catalog_id,
    create_time,
    remark,
    is_delete
  }
}
