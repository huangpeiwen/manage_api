// 判断是否管理员
exports.isAdmin = (req, res, next) => {
    const is_admin = req.auth.is_admin;
    if (!is_admin) {
      return res.new_send("非管理员，暂无操作权限！");
    } else {
      next();
    }
  };
  