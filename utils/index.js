const dayjs = require("dayjs");
const path = require("path");
const fs = require("fs");

// 获取当前日期时间
exports.getNowTime = () => {
  return dayjs().format("YYYY-MM-DD HH:mm:ss");
};

// 获取当前日期
exports.getNowDate = () => {
  return dayjs().format("YYYY-MM-DD");
};

// 格式化时间
exports.formatTime = (data) => {
  if (!data) return "";
  return dayjs(data).format("YYYY-MM-DD HH:mm:ss");
};

/**
 * 删除文件
 * @param {*} filePath 本地文件路径
 */
exports.deleteFile = (filePath) => {
  // 路径非完整路径，而是本地文件路径
  if (!filePath.includes("http")) {
    let newFilePath = path.join(__dirname, "../public", filePath);
    if (filePath.indexOf("default") === -1) {
      if (fs.existsSync(newFilePath)) {
        fs.unlinkSync(newFilePath);
      }
    }
  }
};
