# manage_api

```bash
1.初始化
git init
2.查看当前状态
git status
3.提交到暂存区
git add .
4.查询当前状态
git status
5.提交到本地的版本库
git commit -m "first commit"
6.第一次需要关联远程仓库
git remote add origin https://gitee.com/huangpeiwen/manage-admin.git
7.提交代码到远程仓库
git push -u origin "master"
8.将远程指定分支 拉取到 本地当前分支上
git pull # 将与本地当前分支同名的远程分支 拉取到 本地当前分支上
# git pull origin <远程分支名>:<本地分支名> # 将远程指定分支 拉取到 本地指定分支上
git pull -u origin "master" # git pull origin <远程分支名>
9.将项目克隆到本地
git clone https://gitee.com/huangpeiwen/manage-admin.git
```



## 一、后端菜单

### 用户管理

- 用户列表
- 

### 编程导航

- 网站导航
- 博客笔记
- 代码片段
- 开发软件
- 书籍推荐
- 在线工具
- 源码资源
- 视频教程
- 编程社区
- 办公协作
- 人工智能

### 娱乐导航

- 电影
- 音乐
- 动漫
- 小说
- 摄影

### 办公写作

- 在线文档
- 思维导图
- 人工智能
- 写作文案
- 随笔日记

### 购物比价

- 商品比价
- 购物软件
- 购物网站
- 已购流水
- 物品导购

### 系统管理

- 目录设置
- 分类设置
- 标签设置
- 字典库（主从表(目录、数据)）
- 字段库（常用字段编码）
- 图片库（系统上传的图片资源）
- 登录日志
- 操作日志





## 二、数据库表

以mysql8.*为主

### 2.1、用户管理

#### 2.1.1、用户管理表

```sql
CREATE TABLE `sys_user` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `username` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户名',
  `password` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '密码',
  `nick_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '昵称',
  `sex` tinyint(1) DEFAULT NULL COMMENT '性别(1:男 2:女)',
  `email` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '手机号',
  `avatar` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '头像',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态(0:禁用 1:正常)',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注(用户说明)',
  `is_admin` tinyint(1) DEFAULT NULL COMMENT '是否管理员',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '是否删除(1:已删除 0:正常)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户管理表';
```

### 2.2、系统管理

#### 2.2.1、菜单管理表

```sql
CREATE TABLE `sys_menu` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `parent_id` int DEFAULT NULL COMMENT '父菜单ID',
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '菜单路径(如:/sys/user)',
  `type` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类型(目录、菜单)',
  `is_operate` tinyint(1) DEFAULT NULL COMMENT '是否编辑权限(系统内置菜单不可编辑)',
  `icon` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '菜单图标',
  `order_num` int DEFAULT NULL COMMENT '排序',
  `is_mobile` tinyint(1) DEFAULT NULL COMMENT '是否移动端',
  `is_pc` tinyint(1) DEFAULT NULL COMMENT '是否电脑端',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注(用户说明)',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '是否删除(0:已删除 1:正常)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='菜单管理表';
```



#### 2.2.2、目录设置

sys_catalog(前端 后端 移动端 数据库端 服务端 软件端 工具端 素材端)

#### 2.2.3、分类设置

sys_catalog_sort

#### 2.2.3、标签设置

sys_tag

#### 字典库

sys_dictionary

sys_dictionary_data

【操作系统、图片分类、】

#### 字段库

sys_field

#### 文件库

sys_file

#### 登录日志



#### 操作日志



### 编程导航

#### 	网站导航

​			前端 (

​					vue

​					react

​					...

​				)

​			后端（java .net core ....）

​			移动端（ios an webapp）

​			数据库端

​			服务端

​			软件端（macos  win linux）

​			工具端

​			素材端（

​				图库

​				）

​			设计端

​			

​			娱乐端（电影 音乐 动漫 小说 游戏）

​			摄影端

​			源码端

​			代码片段

​			编程社区

​			办公协作

​			人工智能



blog_website

#### 	博客笔记

blog_note

#### 	代码片段

blog_fragment

#### 	开发软件

blog_software

#### 	书籍推荐

blog_book

#### 	在线工具

blog_utool

#### 	源码资源

blog_code

#### 	视频教程

blog_video_tutorial

#### 	编程社区

blog_community

### 娱乐导航

#### 	电影

{电影网站（fun_movie_website）、电影收藏（fun_movie_collect）}

#### 	音乐

{音乐网站（fun_music_website）、音乐收藏（fun_music_collect）}

#### 	动漫

{动漫网站（fun_anime_website）、动漫收藏（fun_anime_collect）}

#### 	小说

{小说网站（fun_novel_website）、小说收藏（fun_novel_collect）}

#### 	摄影

{摄影网站（fun_photo_website）、摄影收藏（fun_photo_collect）}

### 办公协作

#### 办公软件

office_software

#### 	在线文档

office_website_doc

腾讯文档、有道云文档、云雀

#### 	思维导图

office_mind_guide

#### 	人工智能



#### 	写作文案

#### 	随笔日记





分类管理表

标签管理表

字典库表(主从表) 主表：目录 从表（比如品牌，颜色等）左表右表

字段表(记录常用字段和编码)

账号表(收藏各大网站的账号和密码)

登录日志表

操作日志表

网站导航表

博客笔记表

代码片段表

开发软件表

书籍推荐表

在线工具表

源码资源表

视频教程表

编程社区表

娱乐导航表

在线文档表

思维导图表

物品导购（主从）

人工智能表

写作文案表

随笔日记表

