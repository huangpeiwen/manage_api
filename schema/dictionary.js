// 导入 joi 模块
const joi = require('joi')

// 主键Id
const id = joi.number().integer().required().messages({
  'number.base': '字典ID类型错误',
  'any.required': '字典ID必填'
})

//  字典编码
const code = joi.string().min(1).max(64).required().messages({
  'any.required': '字典编码必填',
  'string.max': '字典编码长度错误(64)'
})

//  字典名称
const name = joi.string().min(1).max(64).required().messages({
  'any.required': '字典名称必填',
  'string.max': '字典名称长度错误(64)'
})

// 用户Id
const user_id = joi.number().integer().allow(null)

// 创建时间
const create_time = joi.date() //.format("YYYY-MM-DD HH:mm:ss");

// 备注
const remark = joi.string().allow(null, '')

// 是否删除
const is_delete = joi.number().integer().default(0)

// 新增验证规则(字典表)
exports.add_dictionary_schema = {
  body: {
    code,
    name,
    user_id,
    create_time,
    remark,
    is_delete
  }
}

// 更新验证规则(字典表)
exports.update_dictionary_schema = {
  body: {
    id,
    code,
    name,
    user_id,
    create_time,
    remark,
    is_delete
  }
}

// 字典数据补充
const dictionary_id = joi.number().integer().required().messages({
  'number.base': '字典ID类型错误',
  'any.required': '字典ID必填'
})

//  数据名
const data_key = joi.string().min(1).max(255).required().messages({
  'any.required': '数据名必填',
  'string.max': '数据名长度错误(255)'
})

//  数据值
const data_value = joi.string().min(1).max(255).required().messages({
  'any.required': '数据值必填',
  'string.max': '数据值长度错误(255)'
})

// 新增验证规则(字典数据表)
exports.add_dictionary_data_schema = {
  body: {
    dictionary_id,
    data_key,
    data_value,
    user_id,
    create_time,
    remark,
    is_delete
  }
}

// 更新验证规则(字典数据表)
exports.update_dictionary_data_schema = {
  body: {
    id,
    dictionary_id,
    data_key,
    data_value,
    user_id,
    create_time,
    remark,
    is_delete
  }
}
