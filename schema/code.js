// 导入 joi 模块
const joi = require('joi')

// 主键Id
const id = joi.number().integer().required().messages({
  'number.base': '代码ID类型错误',
  'any.required': '代码ID必填'
})

// 代码封面
const cover = joi.string().allow(null, '')

// 代码名称
const name = joi.string().min(1).max(255).required().messages({
  'any.required': '代码名称必填',
  'string.max': '代码名称长度错误(128)'
})

// 代码内容
const content = joi.string().allow(null, '')

// 代码地址
const code_url = joi.string().min(1).required().messages({
  'any.required': '代码地址必填'
})

// 原作者
const author = joi.string().allow(null, '')

// gitee地址
const gitee_url = joi.string().allow(null, '')

// github地址
const github_url = joi.string().allow(null, '')

// 目录IDS
const catalog_ids = joi.string().min(1).required().messages({
  'any.required': '目录必填'
})

// 分类IDS
const sort_ids = joi.string().min(1).required().messages({
  'any.required': '分类必填'
})

// 标签IDS
const tag_ids = joi.string().min(1).required().messages({
  'any.required': '标签必填'
})

// 备注
const remark = joi.string().allow(null, '')

// 用户Id
const user_id = joi.number().integer().allow(null)

// 创建时间
const create_time = joi.date().allow(null, '')

// 是否热门网站
const is_hot = joi.number().integer().default(0)

// 是否删除
const is_delete = joi.number().integer().default(0)

// 新增验证规则(代码片段表)
exports.add_code_schema = {
  body: {
    cover,
    name,
    content,
    code_url,
    author,
    gitee_url,
    github_url,
    catalog_ids,
    sort_ids,
    tag_ids,
    remark,
    user_id,
    create_time,
    is_hot,
    is_delete
  }
}

// 更新验证规则(代码片段表)
exports.update_code_schema = {
  body: {
    id,
    cover,
    name,
    content,
    code_url,
    author,
    gitee_url,
    github_url,
    catalog_ids,
    sort_ids,
    tag_ids,
    remark,
    user_id,
    create_time,
    is_hot,
    is_delete
  }
}

// 主表Id
const code_id = joi.number().integer().required().messages({
  'number.base': '主表ID类型错误',
  'any.required': '主表ID必填'
})

// 新增验证规则(博客代码从表)
exports.add_code_url_schema = {
  body: {
    code_id,
    code_url,
    author,
    create_time,
    is_delete
  }
}

// 更新验证规则(博客代码从表)
exports.update_code_url_schema = {
  body: {
    id,
    code_id,
    code_url,
    author,
    create_time,
    is_delete
  }
}
