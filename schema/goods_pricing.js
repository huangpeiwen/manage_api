// 导入 joi 模块
const joi = require('joi')

// 主键Id
const id = joi.number().integer().required().messages({
  'number.base': '流水ID类型错误',
  'any.required': '流水ID必填'
})

// 主表Id
const guide_id = joi.number().integer().required().messages({
  'number.base': '主表ID类型错误',
  'any.required': '主表ID必填'
})

// 当前价格
const current_price = joi.number().allow(null, 0)

// 当前时间
const current_time = joi.date()

// 创建时间
const create_time = joi.date()

// 是否删除
const is_delete = joi.number().integer().default(0)

// 新增验证规则(已购流水表)
exports.add_goods_pricing_schema = {
  body: {
    guide_id,
    current_price,
    current_time,
    create_time,
    is_delete
  }
}

// 更新验证规则(已购流水表)
exports.update_goods_pricing_schema = {
  body: {
    id,
    guide_id,
    current_price,
    current_time,
    create_time,
    is_delete
  }
}
