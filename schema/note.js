// 导入 joi 模块
const joi = require('joi')

// 主键Id
const id = joi.number().integer().required().messages({
  'number.base': '笔记ID类型错误',
  'any.required': '笔记ID必填'
})

// 笔记封面
const cover = joi.string().allow(null, '')

// 笔记名称
const name = joi.string().min(1).max(255).required().messages({
  'any.required': '笔记名称必填',
  'string.max': '笔记名称长度错误(128)'
})

// 笔记内容
const content = joi.string().allow(null, '')

// 目录IDS
const catalog_ids = joi.string().min(1).required().messages({
  'any.required': '目录必填'
})

// 分类IDS
const sort_ids = joi.string().min(1).required().messages({
  'any.required': '分类必填'
})

// 标签IDS
const tag_ids = joi.string().min(1).required().messages({
  'any.required': '标签必填'
})

// 原文链接
const blog_url = joi.string().allow(null, '')

// 原作者
const author = joi.string().allow(null, '')

// 备注
const remark = joi.string().allow(null, '')

// 用户Id
const user_id = joi.number().integer().allow(null)

// 创建时间
const create_time = joi.date().allow(null, '')

// 是否热门网站
const is_hot = joi.number().integer().default(0)

// 是否删除
const is_delete = joi.number().integer().default(0)

// 新增验证规则(博客笔记表)
exports.add_note_schema = {
  body: {
    cover,
    name,
    content,
    catalog_ids,
    sort_ids,
    tag_ids,
    blog_url,
    author,
    remark,
    user_id,
    create_time,
    is_hot,
    is_delete
  }
}

// 更新验证规则(博客笔记表)
exports.update_note_schema = {
  body: {
    id,
    cover,
    name,
    content,
    catalog_ids,
    sort_ids,
    tag_ids,
    blog_url,
    author,
    remark,
    user_id,
    create_time,
    is_hot,
    is_delete
  }
}

// 主键Id
const note_id = joi.number().integer().required().messages({
  'number.base': '主表ID类型错误',
  'any.required': '主表ID必填'
})

// 新增验证规则(博客笔记从表)
exports.add_note_url_schema = {
  body: {
    note_id,
    blog_url,
    author,
    create_time,
    is_delete
  }
}

// 更新验证规则(博客笔记从表)
exports.update_note_url_schema = {
  body: {
    id,
    note_id,
    blog_url,
    author,
    create_time,
    is_delete
  }
}
