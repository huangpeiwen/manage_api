// 导入 joi 模块
const joi = require('joi')

// 主键Id
const id = joi.number().integer().required().messages({
  'number.base': '字段ID类型错误',
  'any.required': '字段ID必填'
})

// 字段编码
const code = joi.string().min(1).max(128).required().messages({
  'any.required': '字段编码必填',
  'string.max': '字段编码长度错误(128)'
})

// 字段名称
const name = joi.string().min(1).max(128).required().messages({
  'any.required': '字段名称必填',
  'string.max': '字段名称长度错误(128)'
})

// 用户Id
const user_id = joi.number().integer().allow(null)

// 创建时间
const create_time = joi.date() //.format("YYYY-MM-DD HH:mm:ss");

// 备注
const remark = joi.string().allow(null, '')

// 是否删除
const is_delete = joi.number().integer().default(0)

// 新增验证规则
exports.add_field_schema = {
  body: {
    code,
    name,
    user_id,
    create_time,
    remark,
    is_delete
  }
}

// 更新验证规则
exports.update_field_schema = {
  body: {
    id,
    code,
    name,
    user_id,
    create_time,
    remark,
    is_delete
  }
}
