// 导入 joi 模块
const joi = require('joi')

// 主键Id
const id = joi.number().integer().required().messages({
  'number.base': '用户ID类型错误',
  'any.required': '用户ID必填'
})

// 用户名规则
const username = joi.string().alphanum().min(2).max(32).required().messages({
  'any.required': '用户名必填',
  'string.alphanum': '用户名只能包含a-zA-Z0-9',
  'string.min': '用户名长度不能小于2',
  'string.max': '用户名长度不能大于16'
})

// 密码规则
const password = joi
  .string()
  .pattern(/^[\S]{3,12}$/)
  .required()
  .messages({
    'any.required': '密码必填',
    'string.pattern.base': '密码长度3-12'
  })

// 密码验证规则
const password_verify = joi
  .string()
  .pattern(/^[\S]{3,12}$/)
  .required()
  .messages({
    'any.required': '密码必填',
    'string.pattern.base': '密码长度3-12'
  })

// 邮箱
const email = joi.string().email().required().messages({
  'any.required': '邮箱必填',
  'string.email': '邮箱格式错误'
})

// 昵称
const nick_name = joi.string().max(32).required().messages({
  'any.required': '昵称必填',
  'string.max': '昵称长度错误(32)'
})

// 性别
const sex = joi.number().integer().allow(null).default(0)

// 手机号
const mobile = joi.string().allow(null, '')

// 头像
const avatar = joi.string().allow(null, '')

// 创建时间
const create_time = joi.date() //.format("YYYY-MM-DD HH:mm:ss");

// 状态
const status = joi.number().integer().default(1)

// 备注
const remark = joi.string().allow(null, '')

// 是否管理员
const is_admin = joi.number().integer().default(0)

// 是否删除
const is_delete = joi.number().integer().default(0)

// 定义用户注册验证规则
exports.reguser_schema = {
  body: {
    username,
    email,
    password,
    password_verify
  }
}

// 定义用户登录验证规则
exports.login_schema = {
  body: {
    username,
    password
  }
}

// 验证规则对象 - 更新密码
exports.update_password_schema = {
  body: {
    new_password: password,
    old_password: password
  }
}

// 定义新增用户验证规则
exports.add_user_schema = {
  body: {
    username,
    password,
    nick_name,
    sex,
    email,
    mobile,
    avatar,
    create_time,
    status,
    remark,
    is_admin,
    is_delete
  }
}

// 定义修改用户验证规则
exports.update_user_schema = {
  body: {
    id,
    username,
    nick_name,
    sex,
    email,
    mobile,
    avatar,
    create_time,
    status,
    remark,
    is_admin,
    is_delete
  }
}
