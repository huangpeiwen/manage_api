// 导入 joi 模块
const joi = require('joi')

// 主键Id
const id = joi.number().integer().required().messages({
  'number.base': '书籍ID类型错误',
  'any.required': '书籍ID必填'
})

// 商品图片
const good_pic = joi.string().allow(null, '')

// 商品名称
const good_name = joi.string().min(1).max(255).required().messages({
  'any.required': '商品名称必填',
  'string.max': '商品名称长度错误(255)'
})

// 天猫地址
const tmall_url = joi.string().allow(null, '')

// 天猫价格
const tmall_price = joi.number().allow(null, 0)

// 淘宝地址
const taobao_url = joi.string().allow(null, '')

// 淘宝价格
const taobao_price = joi.number().allow(null, 0)

// 京东地址
const jd_url = joi.string().allow(null, '')

// 京东价格
const jd_price = joi.number().allow(null, 0)

// 拼多多地址
const pdd_url = joi.string().allow(null, '')

// 拼多多价格
const pdd_price = joi.number().allow(null, 0)

// 品牌
const brand = joi.string().allow(null, '')

// 备注(商品说明)
const remark = joi.string().allow(null, '')

// 用户Id
const user_id = joi.number().integer().allow(null)

// 创建时间
const create_time = joi.date()

// 是否删除
const is_delete = joi.number().integer().default(0)

// 新增验证规则(商品导购表)
exports.add_guide_schema = {
  body: {
    good_pic,
    good_name,
    tmall_url,
    tmall_price,
    taobao_url,
    taobao_price,
    jd_url,
    jd_price,
    pdd_url,
    pdd_price,
    brand,
    remark,
    user_id,
    create_time,
    is_delete
  }
}

// 更新验证规则(商品导购表)
exports.update_guide_schema = {
  body: {
    id,
    good_pic,
    good_name,
    tmall_url,
    tmall_price,
    taobao_url,
    taobao_price,
    jd_url,
    jd_price,
    pdd_url,
    pdd_price,
    brand,
    remark,
    user_id,
    create_time,
    is_delete
  }
}
