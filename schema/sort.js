// 导入 joi 模块
const joi = require('joi')

// 主键Id
const id = joi.number().integer().required().messages({
  'number.base': '分类ID类型错误',
  'any.required': '分类ID必填'
})

// 分类图标
const icon = joi.string().allow(null, '')

// 分类编码
const code = joi.string().min(1).max(64).required().messages({
  'any.required': '分类编码必填',
  'string.max': '分类编码长度错误(64)'
})

// 分类名称
const name = joi.string().min(1).max(64).required().messages({
  'any.required': '分类名称必填',
  'string.max': '分类名称长度错误(64)'
})

// 用户Id
const user_id = joi.number().integer().allow(null)

// 创建时间
const create_time = joi.date()

// 备注
const remark = joi.string().allow(null, '')

// 是否热门分类
const is_hot = joi.number().integer().default(0)

// 是否删除
const is_delete = joi.number().integer().default(0)

// 新增验证规则(分类表)
exports.add_sort_schema = {
  body: {
    icon,
    code,
    name,
    user_id,
    create_time,
    remark,
    is_hot,
    is_delete
  }
}

// 更新验证规则(分类表)
exports.update_sort_schema = {
  body: {
    id,
    icon,
    code,
    name,
    user_id,
    create_time,
    remark,
    is_hot,
    is_delete
  }
}