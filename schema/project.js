// 导入 joi 模块
const joi = require('joi')

// 主键Id
const id = joi.number().integer().required().messages({
  'number.base': '项目ID类型错误',
  'any.required': '项目ID必填'
})

// 项目封面
const cover = joi.string().allow(null, '')

// 项目名称
const name = joi.string().min(1).max(128).required().messages({
  'any.required': '项目名称必填',
  'string.max': '项目名称长度错误(128)'
})

// 项目内容
const content = joi.string().allow(null, '')

// 内置语言
const language = joi.string().min(1).required().messages({
  'any.required': '内置语言必填'
})

// 预览地址
const preview_url = joi.string().allow(null, '')

// 原作者
const author = joi.string().allow(null, '')

// gitee地址
const gitee_url = joi.string().allow(null, '')

// github地址
const github_url = joi.string().allow(null, '')

// 目录IDS
const catalog_ids = joi.string().min(1).required().messages({
  'any.required': '目录必填'
})

// 分类IDS
const sort_ids = joi.string().min(1).required().messages({
  'any.required': '分类必填'
})

// 标签IDS
const tag_ids = joi.string().min(1).required().messages({
  'any.required': '标签必填'
})

// 账号
const account = joi.string().allow(null, '')

// 密码
const password = joi.string().allow(null, '')

// 备注
const remark = joi.string().allow(null, '')

// 用户Id
const user_id = joi.number().integer().allow(null)

// 创建时间
const create_time = joi.date().allow(null, '') //.format("YYYY-MM-DD HH:mm:ss");

// 是否热门网站
const is_hot = joi.number().integer().default(0)

// 是否删除
const is_delete = joi.number().integer().default(0)

// 新增验证规则
exports.add_project_schema = {
  body: {
    cover,
    name,
    content,
    language,
    preview_url,
    author,
    gitee_url,
    github_url,
    catalog_ids,
    sort_ids,
    tag_ids,
    account,
    password,
    remark,
    user_id,
    create_time,
    is_hot,
    is_delete
  }
}

// 更新验证规则
exports.update_project_schema = {
  body: {
    id,
    cover,
    name,
    content,
    language,
    preview_url,
    author,
    gitee_url,
    github_url,
    catalog_ids,
    sort_ids,
    tag_ids,
    account,
    password,
    remark,
    user_id,
    create_time,
    is_hot,
    is_delete
  }
}
