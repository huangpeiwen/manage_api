// 导入 joi 模块
const joi = require('joi')

// 主键Id
// const id = joi.number().integer().required().messages({
//   "number.base": "主键ID类型错误",
//   "any.required": "主键ID必填",
// });

// 业务主表ID
const biz_id = joi.number().integer().required().messages({
  'number.base': '业务ID类型错误',
  'any.required': '业务ID必填'
})

// 文件路径
const file_url = joi.string().min(1).max(512).required().messages({
  'any.required': '文件路径必填',
  'string.max': '文件路径长度错误(512)'
})

// 文件名称
const file_name = joi.string().min(1).max(255).required().messages({
  'any.required': '文件名称必填',
  'string.max': '文件名称长度错误(512)'
})

// 文件类型
const file_type = joi.string().min(1).max(64).required().messages({
  'any.required': '文件类型必填',
  'string.max': '文件类型长度错误(64)'
})

// 文件大小
const file_size = joi.number().required().messages({
  'any.required': '文件大小必填'
})

// 文件扩展名
const file_extname = joi.string().min(1).max(32).required().messages({
  'any.required': '文件扩展名必填',
  'string.max': '文件扩展名长度错误(32)'
})

// 用户Id
const user_id = joi.number().integer().allow(null)

// 创建时间
const create_time = joi.date() //.format("YYYY-MM-DD HH:mm:ss");

// 备注
const remark = joi.string().allow(null, '')

// 是否删除
const is_delete = joi.number().integer().default(0)

// 新增验证规则
exports.add_file_schema = {
  body: {
    biz_id,
    file_url,
    file_name,
    file_type,
    file_size,
    file_extname,
    user_id,
    create_time,
    remark,
    is_delete
  }
}
