// 导入 joi 模块
const joi = require('joi')

// 主键Id
const id = joi.number().integer().required().messages({
  'number.base': '目录ID类型错误',
  'any.required': '目录ID必填'
})

// 目录图标
const icon = joi.string().allow(null, '')

// 目录编码
const code = joi.string().min(1).max(64).required().messages({
  'any.required': '目录编码必填',
  'string.max': '目录编码长度错误(64)'
})

// 目录名称
const name = joi.string().min(1).max(64).required().messages({
  'any.required': '目录名称必填',
  'string.max': '目录名称长度错误(64)'
})

// 用户Id
const user_id = joi.number().integer().allow(null)

// 创建时间
const create_time = joi.date().allow(null, '') //.format("YYYY-MM-DD HH:mm:ss");

// 备注
const remark = joi.string().allow(null, '')

// 是否删除
const is_delete = joi.number().integer().default(0)

// 新增验证规则(目录表)
exports.add_catalog_schema = {
  body: {
    icon,
    code,
    name,
    user_id,
    create_time,
    remark,
    is_delete
  }
}

// 更新验证规则(目录表)
exports.update_catalog_schema = {
  body: {
    id,
    code,
    name,
    icon,
    user_id,
    create_time,
    remark,
    is_delete
  }
}
