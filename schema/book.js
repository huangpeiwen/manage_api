// 导入 joi 模块
const joi = require('joi')

// 主键Id
const id = joi.number().integer().required().messages({
  'number.base': '书籍ID类型错误',
  'any.required': '书籍ID必填'
})

// 封面
const cover = joi.string().allow(null, '')

// 书名
const name = joi.string().min(1).max(128).required().messages({
  'any.required': '项目名称必填',
  'string.max': '项目名称长度错误(128)'
})

// 作者
const author = joi.string().allow(null, '')

// 出版社
const publisher = joi.string().allow(null, '')

// 目录IDS
const catalog_ids = joi.string().min(1).required().messages({
  'any.required': '目录必填'
})

// 分类IDS
const sort_ids = joi.string().min(1).required().messages({
  'any.required': '分类必填'
})

// 标签IDS
const tag_ids = joi.string().min(1).required().messages({
  'any.required': '标签必填'
})

// 备注(书籍说明)
const remark = joi.string().allow(null, '')

// 用户Id
const user_id = joi.number().integer().allow(null)

// 创建时间
const create_time = joi.date()

// 是否热门网站
const is_hot = joi.number().integer().default(0)

// 是否删除
const is_delete = joi.number().integer().default(0)

// 新增验证规则(书籍推荐表)
exports.add_book_schema = {
  body: {
    cover,
    name,
    author,
    publisher,
    catalog_ids,
    sort_ids,
    tag_ids,
    remark,
    user_id,
    create_time,
    is_hot,
    is_delete
  }
}

// 更新验证规则(书籍推荐表)
exports.update_book_schema = {
  body: {
    id,
    cover,
    name,
    author,
    publisher,
    catalog_ids,
    sort_ids,
    tag_ids,
    remark,
    user_id,
    create_time,
    is_hot,
    is_delete
  }
}
