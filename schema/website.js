// 导入 joi 模块
const joi = require('joi')

// 主键Id
const id = joi.number().integer().required().messages({
  'number.base': '网站ID类型错误',
  'any.required': '网站ID必填'
})

// 网站图标(LOGO)
const icon = joi.string().allow(null, '')

// 网站名称
const name = joi.string().min(1).max(255).required().messages({
  'any.required': '网站名称必填',
  'string.max': '网站名称长度错误(255)'
})

// 网站地址
const url = joi.string().min(1).max(512).required().messages({
  'any.required': '网站地址必填',
  'string.max': '网站地址长度错误(512)'
})

// gitee地址
const gitee_url = joi.string().allow(null, '')

// github地址
const github_url = joi.string().allow(null, '')

// 目录IDS
const catalog_ids = joi.string().min(1).required().messages({
  'any.required': '目录必填'
})

// 分类IDS
const sort_ids = joi.string().min(1).required().messages({
  'any.required': '分类必填'
})

// 标签IDS
const tag_ids = joi.string().min(1).required().messages({
  'any.required': '标签必填'
})

// 是否支持电脑
const is_pc = joi.number().integer().default(0)

// 是否支持手机
const is_mobile = joi.number().integer().default(0)

// 是否支持下载
const is_download = joi.number().integer().default(0)

// 账号
const account = joi.string().allow(null, '')

// 密码
const password = joi.string().allow(null, '')

// 是否开通会员
const is_vip = joi.number().integer().default(0)

// 会员开始日期
const vip_start = joi.date().allow(null, '')

// 会员结束日期
const vip_end = joi.date().allow(null, '')

// 备注
const remark = joi.string().allow(null, '')

// 用户Id
const user_id = joi.number().integer().allow(null)

// 创建时间
const create_time = joi.date().allow(null, '')

// 是否热门网站
const is_hot = joi.number().integer().default(0)

// 是否删除
const is_delete = joi.number().integer().default(0)

// 新增验证规则(网站导航表)
exports.add_website_schema = {
  body: {
    icon,
    name,
    url,
    gitee_url,
    github_url,
    catalog_ids,
    sort_ids,
    tag_ids,
    is_pc,
    is_mobile,
    is_download,
    account,
    password,
    is_vip,
    vip_start,
    vip_end,
    remark,
    user_id,
    create_time,
    is_hot,
    is_delete
  }
}

// 更新验证规则(网站导航表)
exports.update_website_schema = {
  body: {
    id,
    icon,
    name,
    url,
    gitee_url,
    github_url,
    catalog_ids,
    sort_ids,
    tag_ids,
    is_pc,
    is_mobile,
    is_download,
    account,
    password,
    is_vip,
    vip_start,
    vip_end,
    remark,
    user_id,
    create_time,
    is_hot,
    is_delete
  }
}
