// 导入 joi 模块
const joi = require('joi')

// 主键Id
const id = joi.number().integer().required().messages({
  'number.base': '软件ID类型错误',
  'any.required': '软件ID必填'
})

// 软件名称
const name = joi.string().min(1).max(128).required().messages({
  'any.required': '软件名称必填',
  'string.max': '软件名称长度错误(128)'
})

// 软件版本
const version = joi.string().allow(null, '')

// 内置语言
const language = joi.string().min(1).required().messages({
  'any.required': '内置语言必填'
})

// 软件内容
const content = joi.string().allow(null, '')

// 软件分类
const type = joi.string().allow(null, '')

// 操作系统
const system = joi.string().min(1).required().messages({
  'any.required': '操作系统必填'
})

// 软件下载地址
const software_url = joi.string().min(1).required().messages({
  'any.required': '操作系统必填'
})

// 阿里云盘下载地址
const ali_url = joi.string().allow(null, '')

// 阿里云盘下载密钥
const ali_pwd = joi.string().allow(null, '')

// 百度云盘下载地址
const baidu_url = joi.string().allow(null, '')

// 百度云盘下载密钥
const baidu_pwd = joi.string().allow(null, '')

// 其他云盘下载地址
const other_url = joi.string().allow(null, '')

// 其他云盘下载密钥
const other_pwd = joi.string().allow(null, '')

// 是否免费
const is_free = joi.number().integer().default(0)

// 会员账号
const vip_account = joi.string().allow(null, '')

// 会员密码
const vip_password = joi.string().allow(null, '')

// 授权密钥
const secret_key = joi.string().allow(null, '')

// 备注
const remark = joi.string().allow(null, '')

// 用户Id
const user_id = joi.number().integer().allow(null)

// 创建时间
const create_time = joi.date().allow(null, '')

// 是否热门网站
const is_hot = joi.number().integer().default(0)

// 是否删除
const is_delete = joi.number().integer().default(0)

// 新增验证规则(软件管理表)
exports.add_software_schema = {
  body: {
    name,
    version,
    language,
    content,
    type,
    system,
    software_url,
    ali_url,
    ali_pwd,
    baidu_url,
    baidu_pwd,
    other_url,
    other_pwd,
    is_free,
    vip_account,
    vip_password,
    secret_key,
    remark,
    user_id,
    create_time,
    is_hot,
    is_delete
  }
}

// 更新验证规则(软件管理表)
exports.update_software_schema = {
  body: {
    id,
    name,
    version,
    language,
    content,
    type,
    system,
    software_url,
    ali_url,
    ali_pwd,
    baidu_url,
    baidu_pwd,
    other_url,
    other_pwd,
    is_free,
    vip_account,
    vip_password,
    secret_key,
    remark,
    user_id,
    create_time,
    is_hot,
    is_delete
  }
}
