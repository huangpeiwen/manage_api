// 标签管理 sys_tag
const express = require('express')
const router = express.Router()
const expressJoi = require('@escook/express-joi')
// 校验数据
const { add_tag_schema, update_tag_schema } = require('../schema/tag')
const { isAdmin } = require('../utils/auth')
const {
  addTag,
  updateTag,
  deleteTagById,
  getTagLsit,
  getTagById,
  getTagLsitBySortId
} = require('../router_handler/tag')
const { getSortLsitByCatalogId } = require('../router_handler/sort')

// 新增标签
router.post('/tag/add', expressJoi(add_tag_schema), isAdmin, addTag)

// 修改标签
router.put('/tag/update', expressJoi(update_tag_schema), isAdmin, updateTag)

// 删除标签
router.delete('/tag/:id', isAdmin, deleteTagById)

// 获取标签列表
router.get('/tag/list', getTagLsit)

// 获取标签信息
router.get('/tag/:id', getTagById)

// 根据分类Id获取标签列表
router.get('/tag/list/sortid', getTagLsitBySortId)

module.exports = router
