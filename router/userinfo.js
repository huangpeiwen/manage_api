const express = require('express')
const router = express.Router()
const expressJoi = require('@escook/express-joi')

const {
  update_password_schema,
  add_user_schema,
  update_user_schema
} = require('../schema/user')
const { isAdmin } = require('../utils/auth')
const { upload_avatar } = require('../router_handler/upload')

const {
  updatePassword,
  updateAvatar,
  updateUserStatus,
  deleteUserById,
  getUserList,
  getUserById,
  getUserListDel,
  addUser,
  updateUser,
  restoreUserById
} = require('../router_handler/userinfo')

// 修改密码
router.patch(
  '/update/password',
  expressJoi(update_password_schema),
  updatePassword
)

// 修改头像
router.patch('/update/avatar', upload_avatar, updateAvatar)

// 修改用户状态
router.patch('/update/user_status', isAdmin, updateUserStatus)

// 删除用户
router.delete('/delete/user/:id', isAdmin, deleteUserById)

// 获取用户列表
router.get('/user/list', getUserList)

// 获取用户信息
router.get('/user/:id', getUserById)

// 获取用户列表(已删除)
router.get('/user/list/delete', getUserListDel)

// 新增用户信息 , upload_avatar , expressJoi(add_user_schema)
router.post('/user/add', expressJoi(add_user_schema), addUser)

// 修改用户信息
router.put('/user/update', expressJoi(update_user_schema), updateUser)

// 恢复用户
router.patch('/restore/user/:id', isAdmin, restoreUserById)

module.exports = router
