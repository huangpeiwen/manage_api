// 标签管理 sys_tag
const express = require('express')
const router = express.Router()
const expressJoi = require('@escook/express-joi')
// 校验数据
const { add_website_schema, update_website_schema } = require('../schema/website')
const { isAdmin } = require('../utils/auth')
const {
  addWebsite,
  updateWebsite,
  deleteWebsiteById,
  getWebsiteLsit,
  getWebsiteById
} = require('../router_handler/website')

// 新增网站信息
router.post('/website/add', expressJoi(add_website_schema), isAdmin, addWebsite)

// 修改网站信息
router.put('/website/update', expressJoi(update_website_schema), isAdmin, updateWebsite)

// 删除网站信息
router.delete('/website/:id', isAdmin, deleteWebsiteById)

// 获取网站列表
router.get('/website/list', getWebsiteLsit)

// 获取网站信息
router.get('/website/:id', getWebsiteById)

module.exports = router
