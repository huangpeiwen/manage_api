// 未购计价表 buy_goods_flow
const express = require('express')
const router = express.Router()
const expressJoi = require('@escook/express-joi')

// 校验数据
const {
  add_goods_pricing_schema,
  update_goods_pricing_schema
} = require('../schema/goods_pricing')
const { isAdmin } = require('../utils/auth')
const {
  addGoodsPricing,
  updateGoodsPricing,
  deleteGoodsPricingById,
  getGoodsPricingLsit,
  getGoodsPricingById
} = require('../router_handler/goods_pricing')

// 新增未购计价
router.post('/goods_pricing/add', expressJoi(add_goods_pricing_schema), isAdmin, addGoodsPricing)

// 修改未购计价
router.put(
  '/goods_pricing/update',
  expressJoi(update_goods_pricing_schema),
  isAdmin,
  updateGoodsPricing
)

// 删除未购计价
router.delete('/goods_pricing/:id', isAdmin, deleteGoodsPricingById)

// 获取未购计价列表
router.get('/goods_pricing/list', getGoodsPricingLsit)

// 获取未购计价
router.get('/goods_pricing/:id', getGoodsPricingById)

module.exports = router
