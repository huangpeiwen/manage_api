// 文件库 sys_login_log
const express = require("express");
const router = express.Router();

const { getLoginLog } = require("../router_handler/login_log");

// 获取登录日志列表
router.get("/login_log/list", getLoginLog);

// 将路由对象共享出去
module.exports = router;
