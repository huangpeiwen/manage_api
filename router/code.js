// 标签管理 sys_tag
const express = require('express')
const router = express.Router()
const expressJoi = require('@escook/express-joi')
// 校验数据
const {
  add_code_schema,
  update_code_schema,
  add_code_url_schema,
  update_code_url_schema
} = require('../schema/code')
const { isAdmin } = require('../utils/auth')
const {
  addCode,
  updateCode,
  deleteCodeById,
  getCodeLsit,
  getCodeById,
  addCodeUrl,
  updateCodeUrl,
  deleteCodeUrlById,
  getCodeUrlLsit,
  getCodeUrlById
} = require('../router_handler/code')

// 新增博客笔记
router.post('/code/add', expressJoi(add_code_schema), isAdmin, addCode)

// 修改博客笔记
router.put('/code/update', expressJoi(update_code_schema), isAdmin, updateCode)

// 删除博客笔记
router.delete('/code/:id', isAdmin, deleteCodeById)

// 获取笔记列表
router.get('/code/list', getCodeLsit)

// 获取博客笔记
router.get('/code/:id', getCodeById)

// 新增博客笔记从表
router.post(
  '/code_url/add',
  expressJoi(add_code_url_schema),
  isAdmin,
  addCodeUrl
)

// 修改博客笔记从表
router.put(
  '/code_url/update',
  expressJoi(update_code_url_schema),
  isAdmin,
  updateCodeUrl
)

// 删除博客笔记
router.delete('/code_url/:id', isAdmin, deleteCodeUrlById)

// 获取笔记列表
router.get('/code_url/list', getCodeUrlLsit)

// 获取博客笔记
router.get('/code_url/:id', getCodeUrlById)

module.exports = router
