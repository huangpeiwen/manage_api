// 已购流水表 buy_goods_flow
const express = require('express')
const router = express.Router()
const expressJoi = require('@escook/express-joi')

// 校验数据
const {
  add_goods_flow_schema,
  update_goods_flow_schema
} = require('../schema/goods_flow')
const { isAdmin } = require('../utils/auth')
const {
  addGoodsFlow,
  updateGoodsFlow,
  deleteGoodsFlowById,
  getGoodsFlowLsit,
  getGoodsFlowById
} = require('../router_handler/goods_flow')

// 新增已购流水
router.post('/goods_flow/add', expressJoi(add_goods_flow_schema), isAdmin, addGoodsFlow)

// 修改已购流水
router.put(
  '/goods_flow/update',
  expressJoi(update_goods_flow_schema),
  isAdmin,
  updateGoodsFlow
)

// 删除已购流水
router.delete('/goods_flow/:id', isAdmin, deleteGoodsFlowById)

// 获取已购流水列表
router.get('/goods_flow/list', getGoodsFlowLsit)

// 获取已购流水
router.get('/goods_flow/:id', getGoodsFlowById)

module.exports = router
