// 标签管理 sys_tag
const express = require('express')
const router = express.Router()
const expressJoi = require('@escook/express-joi')
// 校验数据
const {
  add_project_schema,
  update_project_schema
} = require('../schema/project')
const { isAdmin } = require('../utils/auth')
const {
  addProject,
  updateProject,
  deleteProjectById,
  getProjectLsit,
  getProjectById
} = require('../router_handler/project')

// 新增项目源码
router.post('/project/add', expressJoi(add_project_schema), isAdmin, addProject)

// 修改项目源码
router.put(
  '/project/update',
  expressJoi(update_project_schema),
  isAdmin,
  updateProject
)

// 删除项目源码
router.delete('/project/:id', isAdmin, deleteProjectById)

// 获取笔记列表
router.get('/project/list', getProjectLsit)

// 获取项目源码
router.get('/project/:id', getProjectById)

module.exports = router
