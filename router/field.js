// 字段管理 sys_field
const express = require("express");
const router = express.Router();
const expressJoi = require("@escook/express-joi");
// 校验数据
const { add_field_schema, update_field_schema } = require("../schema/field");
const { isAdmin } = require("../utils/auth");
const {
  addField,
  updateField,
  deleteFieldById,
  getFieldLsit,
  getFieldById,
} = require("../router_handler/field");

// 新增字段
router.post("/field/add", expressJoi(add_field_schema), isAdmin, addField);

// 修改字段
router.put(
  "/field/update",
  expressJoi(update_field_schema),
  isAdmin,
  updateField
);

// 删除字段
router.delete("/field/:id", isAdmin, deleteFieldById);

// 获取字段列表
router.get("/field/list", getFieldLsit);

// 获取字段信息
router.get("/field/:id", getFieldById);

module.exports = router;
