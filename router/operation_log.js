// 文件库 sys_login_log
const express = require("express");
const router = express.Router();

const { getOperationLog } = require("../router_handler/operation_log");

// 获取操作日志列表
router.get("/operation_log/list", getOperationLog);

// 将路由对象共享出去
module.exports = router;
