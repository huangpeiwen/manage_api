const express = require('express')
const router = express.Router()
const expressJoi = require('@escook/express-joi')
const { regUser, login } = require('../router_handler/user')
const { login_schema, reguser_schema } = require('../schema/user')

// 注册新用户
router.post('/blog/reguser', expressJoi(reguser_schema), regUser)

// 登录
router.post('/blog/login', expressJoi(login_schema), login)

// 将路由对象共享出去
module.exports = router
