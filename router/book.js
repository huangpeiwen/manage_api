// 标签管理 sys_tag
const express = require('express')
const router = express.Router()
const expressJoi = require('@escook/express-joi')
// 校验数据
const {
  add_book_schema,
  update_book_schema
} = require('../schema/book')
const { isAdmin } = require('../utils/auth')
const {
  addBook,
  updateBook,
  deleteBookById,
  getBookLsit,
  getBookById
} = require('../router_handler/book')

// 新增书籍推荐
router.post('/book/add', expressJoi(add_book_schema), isAdmin, addBook)

// 修改书籍推荐
router.put(
  '/book/update',
  expressJoi(update_book_schema),
  isAdmin,
  updateBook
)

// 删除书籍推荐
router.delete('/book/:id', isAdmin, deleteBookById)

// 获取笔记列表
router.get('/book/list', getBookLsit)

// 获取书籍推荐
router.get('/book/:id', getBookById)

module.exports = router
