// 分类管理 sys_Sort sys_Sort_sort
const express = require('express')
const router = express.Router()
const expressJoi = require('@escook/express-joi')
const { add_sort_schema, update_sort_schema } = require('../schema/sort')
const { isAdmin } = require('../utils/auth')
const {
  addSort,
  updateSort,
  deleteSortById,
  getSortLsit,
  getSortById,
  addSortInTag,
  deleteSortInTagById,
  getSortInTagList,
  getSortLsitByCatalogId
} = require('../router_handler/sort')

// 新增分类
router.post('/sort/add', expressJoi(add_sort_schema), isAdmin, addSort)

// 修改分类
router.put('/sort/update', expressJoi(update_sort_schema), isAdmin, updateSort)

// 删除分类
router.delete('/sort/:id', isAdmin, deleteSortById)

// 获取分类列表
router.get('/sort/list', getSortLsit)

// 获取分类信息
router.get('/sort/:id', getSortById)

// 新增分类关联标签
router.post('/sortTag/add', isAdmin, addSortInTag)

// 删除分类关联标签
router.post('/sortTag/delete', isAdmin, deleteSortInTagById)

// 根据分类Id查询标签列表
router.get('/sortTag/list/:id', getSortInTagList)

// 根据目录Id查询分类列表
router.get('/sort/list/catalogid', getSortLsitByCatalogId)

// 将路由对象共享出去
module.exports = router
