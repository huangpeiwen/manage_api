// 标签管理 sys_tag
const express = require('express')
const router = express.Router()
const expressJoi = require('@escook/express-joi')
// 校验数据
const {
  add_note_schema,
  update_note_schema,
  add_note_url_schema,
  update_note_url_schema
} = require('../schema/note')
const { isAdmin } = require('../utils/auth')
const {
  addNote,
  updateNote,
  deleteNoteById,
  getNoteLsit,
  getNoteById,
  addNoteUrl,
  updateNoteUrl,
  deleteNoteUrlById,
  getNoteUrlLsit,
  getNoteUrlById
} = require('../router_handler/note')

// 新增博客笔记
router.post('/note/add', expressJoi(add_note_schema), isAdmin, addNote)

// 修改博客笔记
router.put('/note/update', expressJoi(update_note_schema), isAdmin, updateNote)

// 删除博客笔记
router.delete('/note/:id', isAdmin, deleteNoteById)

// 获取笔记列表
router.get('/note/list', getNoteLsit)

// 获取博客笔记
router.get('/note/:id', getNoteById)

// 新增博客笔记从表
router.post(
  '/note_url/add',
  expressJoi(add_note_url_schema),
  isAdmin,
  addNoteUrl
)

// 修改博客笔记从表
router.put(
  '/note_url/update',
  expressJoi(update_note_url_schema),
  isAdmin,
  updateNoteUrl
)

// 删除博客笔记
router.delete('/note_url/:id', isAdmin, deleteNoteUrlById)

// 获取笔记列表
router.get('/note_url/list', getNoteUrlLsit)

// 获取博客笔记
router.get('/note_url/:id', getNoteUrlById)

module.exports = router
