// 目录管理 sys_catalog sys_catalog_sort
const express = require('express')
const router = express.Router()
const expressJoi = require('@escook/express-joi')
const {
  add_catalog_schema,
  update_catalog_schema,
  add_catalog_sort_schema
} = require('../schema/catalog')
const { isAdmin } = require('../utils/auth')
const {
  addCatalog,
  updateCatalog,
  deleteCatalogById,
  getCatalogLsit,
  getCatalogById,
  addCatalogInSort,
  deleteCatalogInSortById,
  getCatalogInSortList
} = require('../router_handler/catalog')

// 新增目录
router.post('/catalog/add', expressJoi(add_catalog_schema), isAdmin, addCatalog)

// 修改目录
router.put(
  '/catalog/update',
  expressJoi(update_catalog_schema),
  isAdmin,
  updateCatalog
)

// 删除目录
router.delete('/catalog/:id', isAdmin, deleteCatalogById)

// 获取目录列表
router.get('/catalog/list', getCatalogLsit)

// 获取目录信息
router.get('/catalog/:id', getCatalogById)

// 新增目录关联分类
router.post('/catalogSort/add', isAdmin, addCatalogInSort)

// 删除目录关联分类
router.post('/catalogSort/delete', isAdmin, deleteCatalogInSortById)

// 根据目录Id查询分类列表
router.get('/catalogSort/list/:id', getCatalogInSortList)

// 将路由对象共享出去
module.exports = router
