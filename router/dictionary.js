// 字典库 sys_dictionary sys_dictionary_data
const express = require("express");
const router = express.Router();
const expressJoi = require("@escook/express-joi");
const {
  add_dictionary_schema,
  update_dictionary_schema,
  add_dictionary_data_schema,
  update_dictionary_data_schema,
} = require("../schema/dictionary");
const { isAdmin } = require("../utils/auth");
const {
  addDictionary,
  updateDictionary,
  deleteDictionaryById,
  getDictionaryLsit,
  getDictionaryById,
  addDictionaryData,
  updateDictionaryData,
  deleteDictionaryDataById,
  getDictionaryDataLsit,
  getDictionaryDataById,
} = require("../router_handler/dictionary");

// 新增字典
router.post(
  "/dictionary/add",
  expressJoi(add_dictionary_schema),
  isAdmin,
  addDictionary
);

// 修改字典
router.put(
  "/dictionary/update",
  expressJoi(update_dictionary_schema),
  isAdmin,
  updateDictionary
);

// 删除字典
router.delete("/dictionary/:id", isAdmin, deleteDictionaryById);

// 获取字典列表
router.get("/dictionary/list", getDictionaryLsit);

// 获取字典信息
router.get("/dictionary/:id", getDictionaryById);

// 新增字典数据
router.post(
  "/dictionary_data/add",
  expressJoi(add_dictionary_data_schema),
  isAdmin,
  addDictionaryData
);

// 修改字典数据
router.put(
  "/dictionary_data/update",
  expressJoi(update_dictionary_data_schema),
  isAdmin,
  updateDictionaryData
);

// 删除字典数据
router.delete("/dictionary_data/:id", isAdmin, deleteDictionaryDataById);

// 获取字典数据列表
router.get("/dictionary_data/list", getDictionaryDataLsit);

// 获取字典数据信息
router.get("/dictionary_data/:id", getDictionaryDataById);

// 将路由对象共享出去
module.exports = router;
