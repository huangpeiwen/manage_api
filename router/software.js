// 标签管理 sys_tag
const express = require('express')
const router = express.Router()
const expressJoi = require('@escook/express-joi')
// 校验数据
const {
  add_software_schema,
  update_software_schema
} = require('../schema/software')
const { isAdmin } = require('../utils/auth')
const {
  addSoftware,
  updateSoftware,
  deleteSoftwareById,
  getSoftwareLsit,
  getSoftwareById
} = require('../router_handler/software')

// 新增项目源码
router.post('/software/add', expressJoi(add_software_schema), isAdmin, addSoftware)

// 修改项目源码
router.put(
  '/software/update',
  expressJoi(update_software_schema),
  isAdmin,
  updateSoftware
)

// 删除项目源码
router.delete('/software/:id', isAdmin, deleteSoftwareById)

// 获取笔记列表
router.get('/software/list', getSoftwareLsit)

// 获取项目源码
router.get('/software/:id', getSoftwareById)

module.exports = router
