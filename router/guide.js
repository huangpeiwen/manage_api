// 标签管理 sys_tag
const express = require('express')
const router = express.Router()
const expressJoi = require('@escook/express-joi')

// 校验数据
const {
  add_guide_schema,
  update_guide_schema
} = require('../schema/guide')
const { isAdmin } = require('../utils/auth')
const {
  addGuide,
  updateGuide,
  deleteGuideById,
  getGuideLsit,
  getGuideById
} = require('../router_handler/guide')

// 新增商品导购
router.post('/guide/add', expressJoi(add_guide_schema), isAdmin, addGuide)

// 修改商品导购
router.put(
  '/guide/update',
  expressJoi(update_guide_schema),
  isAdmin,
  updateGuide
)

// 删除商品导购
router.delete('/guide/:id', isAdmin, deleteGuideById)

// 获取笔记列表
router.get('/guide/list', getGuideLsit)

// 获取商品导购
router.get('/guide/:id', getGuideById)

module.exports = router
