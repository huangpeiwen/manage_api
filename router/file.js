// 文件库 sys_file
const express = require('express')
const router = express.Router()

const { getFileLsit, getIconLsit } = require('../router_handler/file')

// 获取文件库列表
router.get('/file/list', getFileLsit)

// 获取图标库列表
router.get('/icon/list', getIconLsit)

// 将路由对象共享出去
module.exports = router
